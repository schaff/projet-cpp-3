/********************************************************************************
** Form generated from reading UI file 'end.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_END_H
#define UI_END_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_END
{
public:
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QPushButton *pushButton_2;

    void setupUi(QWidget *END)
    {
        if (END->objectName().isEmpty())
            END->setObjectName(QStringLiteral("END"));
        END->resize(525, 412);
        label = new QLabel(END);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(100, 50, 251, 71));
        label->setStyleSheet(QStringLiteral("font: 14pt \"MS Shell Dlg 2\";"));
        label_2 = new QLabel(END);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(110, 160, 101, 41));
        label_2->setStyleSheet(QLatin1String("font: 8pt \"MS Shell Dlg 2\";\n"
"text-decoration: underline;"));
        label_3 = new QLabel(END);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(150, 240, 211, 51));
        label_4 = new QLabel(END);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(160, 190, 191, 41));
        pushButton_2 = new QPushButton(END);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(190, 330, 93, 28));

        retranslateUi(END);

        QMetaObject::connectSlotsByName(END);
    } // setupUi

    void retranslateUi(QWidget *END)
    {
        END->setWindowTitle(QApplication::translate("END", "Form", Q_NULLPTR));
        label->setText(QApplication::translate("END", "MERCI D'AVOIR JOUER", Q_NULLPTR));
        label_2->setText(QApplication::translate("END", "Cr\303\251dit : ", Q_NULLPTR));
        label_3->setText(QApplication::translate("END", "D\303\251veloppeur : Philippe SCHAFFNER", Q_NULLPTR));
        label_4->setText(QApplication::translate("END", "Sc\303\251nariste : Lo\303\257c SPEISSER", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("END", "Quit", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class END: public Ui_END {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_END_H
