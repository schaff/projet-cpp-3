/********************************************************************************
** Form generated from reading UI file 'messageerror.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MESSAGEERROR_H
#define UI_MESSAGEERROR_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MessageError
{
public:
    QLabel *label;
    QPushButton *pushButton;

    void setupUi(QWidget *MessageError)
    {
        if (MessageError->objectName().isEmpty())
            MessageError->setObjectName(QStringLiteral("MessageError"));
        MessageError->resize(262, 166);
        label = new QLabel(MessageError);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(50, 30, 181, 31));
        pushButton = new QPushButton(MessageError);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(90, 90, 93, 28));

        retranslateUi(MessageError);

        QMetaObject::connectSlotsByName(MessageError);
    } // setupUi

    void retranslateUi(QWidget *MessageError)
    {
        MessageError->setWindowTitle(QApplication::translate("MessageError", "Form", Q_NULLPTR));
        label->setText(QApplication::translate("MessageError", "ERROR : Entrez un pseudo", Q_NULLPTR));
        pushButton->setText(QApplication::translate("MessageError", "ok", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MessageError: public Ui_MessageError {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MESSAGEERROR_H
