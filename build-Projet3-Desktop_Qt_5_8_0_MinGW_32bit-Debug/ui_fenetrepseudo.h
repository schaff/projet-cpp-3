/********************************************************************************
** Form generated from reading UI file 'fenetrepseudo.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FENETREPSEUDO_H
#define UI_FENETREPSEUDO_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_fenetrePseudo
{
public:
    QLabel *label;
    QTextEdit *namePerso;
    QPushButton *btnStartGame;
    QLabel *label_2;

    void setupUi(QWidget *fenetrePseudo)
    {
        if (fenetrePseudo->objectName().isEmpty())
            fenetrePseudo->setObjectName(QStringLiteral("fenetrePseudo"));
        fenetrePseudo->resize(397, 167);
        fenetrePseudo->setStyleSheet(QLatin1String("background-image: url(:/fndEcn/bg-parchemin.jpg);\n"
"background-image: url(:/fndEcn/xE5CD.jpg);"));
        label = new QLabel(fenetrePseudo);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(40, 30, 131, 31));
        namePerso = new QTextEdit(fenetrePseudo);
        namePerso->setObjectName(QStringLiteral("namePerso"));
        namePerso->setGeometry(QRect(170, 30, 181, 31));
        btnStartGame = new QPushButton(fenetrePseudo);
        btnStartGame->setObjectName(QStringLiteral("btnStartGame"));
        btnStartGame->setGeometry(QRect(100, 110, 201, 41));
        btnStartGame->setStyleSheet(QLatin1String("color:rgb(255, 255, 255);\n"
"background-image: url(:/fndBtn/Btn.jpg);\n"
""));
        label_2 = new QLabel(fenetrePseudo);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(120, 70, 171, 31));
        label_2->setStyleSheet(QLatin1String("font: 75 9pt \"MS Shell Dlg 2\";\n"
"text-decoration: underline;\n"
"color: rgb(255, 0, 0);"));

        retranslateUi(fenetrePseudo);

        QMetaObject::connectSlotsByName(fenetrePseudo);
    } // setupUi

    void retranslateUi(QWidget *fenetrePseudo)
    {
        fenetrePseudo->setWindowTitle(QApplication::translate("fenetrePseudo", "Form", Q_NULLPTR));
        label->setText(QApplication::translate("fenetrePseudo", "Entrez votre pseudo : ", Q_NULLPTR));
        btnStartGame->setText(QApplication::translate("fenetrePseudo", "Start", Q_NULLPTR));
        label_2->setText(QApplication::translate("fenetrePseudo", "!!! N\303\251cessite un pseudo", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class fenetrePseudo: public Ui_fenetrePseudo {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FENETREPSEUDO_H
