/********************************************************************************
** Form generated from reading UI file 'fenetrejeu.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FENETREJEU_H
#define UI_FENETREJEU_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FenetreJeu
{
public:
    QPushButton *btnRandom;
    QPushButton *btnOk;
    QLabel *LChoose;
    QLabel *label_3;
    QTextEdit *textEdit;
    QRadioButton *RbChoose1;
    QRadioButton *RbChoose2;
    QRadioButton *RbChoose3;
    QProgressBar *PbPv;
    QLabel *label;
    QLabel *label_2;
    QProgressBar *PbDext;
    QPushButton *pushButton;

    void setupUi(QWidget *FenetreJeu)
    {
        if (FenetreJeu->objectName().isEmpty())
            FenetreJeu->setObjectName(QStringLiteral("FenetreJeu"));
        FenetreJeu->resize(1261, 780);
        FenetreJeu->setStyleSheet(QStringLiteral("background-image: url(:/fndEcn/test.jpg);"));
        btnRandom = new QPushButton(FenetreJeu);
        btnRandom->setObjectName(QStringLiteral("btnRandom"));
        btnRandom->setGeometry(QRect(1030, 340, 161, 141));
        btnRandom->setStyleSheet(QStringLiteral("background-image: url(:/fndEcn/fen.jpg);"));
        btnRandom->setIconSize(QSize(250, 250));
        btnOk = new QPushButton(FenetreJeu);
        btnOk->setObjectName(QStringLiteral("btnOk"));
        btnOk->setGeometry(QRect(1030, 520, 181, 71));
        btnOk->setStyleSheet(QStringLiteral("background-image: url(:/fndEcn/fen.jpg);"));
        LChoose = new QLabel(FenetreJeu);
        LChoose->setObjectName(QStringLiteral("LChoose"));
        LChoose->setGeometry(QRect(20, 520, 981, 51));
        LChoose->setStyleSheet(QStringLiteral("color: rgb(255, 255, 255);"));
        label_3 = new QLabel(FenetreJeu);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(1090, 110, 101, 91));
        label_3->setPixmap(QPixmap(QString::fromUtf8("../../Pictures/db3ad6aeb4cedc0729f70da0d5839913 (2).jpg")));
        textEdit = new QTextEdit(FenetreJeu);
        textEdit->setObjectName(QStringLiteral("textEdit"));
        textEdit->setGeometry(QRect(23, 16, 981, 491));
        QFont font;
        font.setFamily(QStringLiteral("Script MT Bold"));
        font.setPointSize(20);
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(50);
        textEdit->setFont(font);
        textEdit->setStyleSheet(QStringLiteral("background-image: url(:/fndEcn/fen.jpg);"));
        textEdit->setReadOnly(true);
        RbChoose1 = new QRadioButton(FenetreJeu);
        RbChoose1->setObjectName(QStringLiteral("RbChoose1"));
        RbChoose1->setGeometry(QRect(20, 580, 981, 51));
        QFont font1;
        font1.setPointSize(7);
        RbChoose1->setFont(font1);
        RbChoose1->setStyleSheet(QStringLiteral("background-image: url(:/fndEcn/fen.jpg);"));
        RbChoose1->setInputMethodHints(Qt::ImhNone);
        RbChoose1->setChecked(true);
        RbChoose1->setAutoRepeat(true);
        RbChoose2 = new QRadioButton(FenetreJeu);
        RbChoose2->setObjectName(QStringLiteral("RbChoose2"));
        RbChoose2->setGeometry(QRect(20, 640, 981, 51));
        RbChoose2->setFont(font1);
        RbChoose2->setStyleSheet(QStringLiteral("background-image: url(:/fndEcn/fen.jpg);"));
        RbChoose3 = new QRadioButton(FenetreJeu);
        RbChoose3->setObjectName(QStringLiteral("RbChoose3"));
        RbChoose3->setGeometry(QRect(20, 700, 981, 51));
        RbChoose3->setFont(font1);
        RbChoose3->setStyleSheet(QStringLiteral("background-image: url(:/fndEcn/fen.jpg);"));
        PbPv = new QProgressBar(FenetreJeu);
        PbPv->setObjectName(QStringLiteral("PbPv"));
        PbPv->setGeometry(QRect(1040, 240, 171, 23));
        PbPv->setMaximum(130);
        PbPv->setValue(24);
        label = new QLabel(FenetreJeu);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(1050, 280, 81, 16));
        label->setStyleSheet(QStringLiteral("background-color: rgb(226, 226, 226);"));
        label_2 = new QLabel(FenetreJeu);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(1050, 220, 81, 16));
        label_2->setStyleSheet(QStringLiteral("background-color: rgb(226, 226, 226);"));
        PbDext = new QProgressBar(FenetreJeu);
        PbDext->setObjectName(QStringLiteral("PbDext"));
        PbDext->setGeometry(QRect(1040, 300, 171, 23));
        PbDext->setMaximum(150);
        PbDext->setValue(24);
        pushButton = new QPushButton(FenetreJeu);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(1150, 10, 51, 41));
        pushButton->setStyleSheet(QStringLiteral("background-image: url(:/fndEcn/fen.jpg);"));

        retranslateUi(FenetreJeu);

        QMetaObject::connectSlotsByName(FenetreJeu);
    } // setupUi

    void retranslateUi(QWidget *FenetreJeu)
    {
        FenetreJeu->setWindowTitle(QApplication::translate("FenetreJeu", "Blaireau", Q_NULLPTR));
        btnRandom->setText(QApplication::translate("FenetreJeu", "Lancer de d\303\251s", Q_NULLPTR));
        btnOk->setText(QApplication::translate("FenetreJeu", "Suite de l'histoire", Q_NULLPTR));
        LChoose->setText(QString());
        label_3->setText(QString());
        RbChoose1->setText(QApplication::translate("FenetreJeu", "RadioButton", Q_NULLPTR));
        RbChoose2->setText(QApplication::translate("FenetreJeu", "RadioButton", Q_NULLPTR));
        RbChoose3->setText(QApplication::translate("FenetreJeu", "RadioButton", Q_NULLPTR));
        label->setText(QApplication::translate("FenetreJeu", "Dext\303\251rit\303\251 : ", Q_NULLPTR));
        label_2->setText(QApplication::translate("FenetreJeu", "Point de vie :", Q_NULLPTR));
        pushButton->setText(QApplication::translate("FenetreJeu", "Mute", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class FenetreJeu: public Ui_FenetreJeu {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FENETREJEU_H
