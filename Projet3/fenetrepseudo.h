#ifndef FENETREPSEUDO_H
#define FENETREPSEUDO_H

/**
 * \file fenetrepseudo.h
 * \brief Fenetre pseudo
 * \author Philippe.S
 * \version 0.1
 * \date 11/04/2017
 *
 * Fichier H qui controle la fenetre où l'on rentre notre pseudo
 *
 */

#include "sqlite.h"
#include <QWidget>
#include <QMainWindow>

namespace Ui {
class fenetrePseudo;
}

class fenetrePseudo : public QMainWindow
{
    Q_OBJECT

public:
    explicit fenetrePseudo(QMainWindow *parent = 0);
    fenetrePseudo(Sqlite *query);
    ~fenetrePseudo();

private slots:
    void on_btnStartGame_clicked();

private:
    Ui::fenetrePseudo *ui;
    Sqlite *query;
    QString name;
};

#endif // FENETREPSEUDO_H
