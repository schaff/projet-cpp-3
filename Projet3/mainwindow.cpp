#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "fenetrepseudo.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    query = new Sqlite();

    musique = new QMediaPlayer();
    list = new QMediaPlaylist();

    list->addMedia(QUrl("Eminem.mp3"));
    list->setPlaybackMode(QMediaPlaylist::Loop);

    musique->setMedia(list);
    musique->setVolume(25);

    musique->play();

}

MainWindow::~MainWindow()
{
    delete ui;
    delete query;
    delete musique;
    delete list;
}

void MainWindow::on_btnNewGame_clicked()
{
    fenetrePseudo *fentre1 = new fenetrePseudo(query);
    query->createDbTables();
    query->insertHistoire();
    fentre1->show();
    musique->pause();
    this->close();

}

void MainWindow::on_btnQuit_clicked()
{
    QCoreApplication::quit();
}

void MainWindow::on_pushButton_clicked()
{
   if (musique->volume()==0)
   {
       musique->setVolume(25);
   }
   else
   {
       musique->setVolume(0);
   }
}
