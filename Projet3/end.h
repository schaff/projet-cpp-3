#ifndef END_H
#define END_H

/**
 * \file end.h
 * \brief Fenetre end
 * \author Philippe.S
 * \version 0.1
 * \date 11/04/2017
 *
 * Fichier H qui controle la fenetre de fin
 *
 */

#include <QWidget>
#include <QMediaPlayer>
#include <QMediaPlaylist>

namespace Ui {
class END;
}

class END : public QWidget
{
    Q_OBJECT

public:
    explicit END(QWidget *parent = 0);
    ~END();

private slots:

    void on_pushButton_2_clicked();

private:
    Ui::END *ui;
    QMediaPlayer *musique;
    QMediaPlaylist *list;
};

#endif // END_H
