#ifndef GAME_H
#define GAME_H

/**
 * \file Game.h
 * \brief class game
 * \author Philippe.S
 * \version 0.1
 * \date 11/04/2017
 *
 * Fichier H qui controle la class de jeu qui agit sur le joueur
 *
 */

#include "joueur.h"
#include "fenetrejeu.h"

#include <time.h>
#include <stdlib.h>



class Game
{
public:
    Game(FenetreJeu *fn);
    ~Game();
    void situation1();
    void situation5();
    void situation6();
    void situation7();
    void situation7_1();
    void situation8_1();
    void situation8_2();

    bool attaqueCc();
    bool attaqueF();
    bool attaquePr();

    bool rouletteRusse();
    void misEnPLaceRR();

    joueur *gamer;
private:

    FenetreJeu *fn;
    int attaque;
    int attaqueEnnemi;
    bool win;
    int tab[6];
    int nbClick;



};

#endif // GAME_H
