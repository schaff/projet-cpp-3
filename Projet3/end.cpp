#include "end.h"
#include "ui_end.h"

END::END(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::END)
{
    ui->setupUi(this);
    //musique du jeu
    musique = new QMediaPlayer();
    list = new QMediaPlaylist();

    list->addMedia(QUrl("Instru.mp3"));
    list->setPlaybackMode(QMediaPlaylist::Loop);

    musique->setMedia(list);
    musique->setVolume(25);

    musique->play();
}

END::~END()
{
    delete ui;

}

void END::on_pushButton_2_clicked()
{
    this->close();
}
