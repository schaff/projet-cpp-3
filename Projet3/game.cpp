#include "game.h"

Game::Game(FenetreJeu *fn)
{
    gamer = new joueur();
    this->fn = fn;
    srand(time(NULL));
    nbClick=0;
}

Game::~Game()
{
    delete gamer;
    delete fn;
}

bool Game::rouletteRusse()
{
    bool alive=true;
    this->misEnPLaceRR();
    if (tab[nbClick]==1)
    {
        alive = false;
    }
    ++nbClick;
    return alive;
}

void Game::misEnPLaceRR()
{
    int random = rand()%6+1;
    for (int i=0; i<6; i++)
    {
        tab[i]=0;
    }
    tab[random]=1;
    random = rand()%6+1;
    if(tab[random]==0)
    {
        tab[random]=1;
    }
    else if (tab[random]==1)
    {
        if (random == 6)
        {
            random-=1;
        }
        else
        {
            random+=1;
        }
        tab[random]=1;
    }
}

void Game::situation1()
{
    int random = fn->getRandom();
    if (random == 1 || random==2)
    {
        gamer->setPv(gamer->getPv()-5);
    }
    else if (random == 3 || random==4)
    {
        gamer->setPv(gamer->getPv()-3);
        gamer->setDext(gamer->getDext()-5);
    }
    else if (random == 5 || random ==6)
    {
        gamer->setDext(gamer->getDext()+3);
    }
}

void Game::situation6()
{
    int random = fn->getRandom();
    if (random == 1 || random==2 || random==3)
    {
        gamer->setPv(gamer->getPv()-10);
    }
}

void Game::situation7()
{
    gamer->setPv(gamer->getPv()-10);
}

void Game::situation7_1()
{
    gamer->setPv(gamer->getPv()+15);
}

void Game::situation5()
{
    gamer->setDext(gamer->getDext()-5);
    gamer->setDext(gamer->getDext()-2);
    gamer->setDext(gamer->getDext()-5);
    gamer->setPv(gamer->getPv()+3);
    gamer->setDext(gamer->getDext()-5);
    gamer->setDext(gamer->getDext()-5);
}

void Game::situation8_1()
{
    gamer->setPv(gamer->getPv()-10);
}

void Game::situation8_2()
{
    gamer->setPv(gamer->getPv()+15);
}

 bool Game::attaqueCc()
 {
     joueur *ennemi = new joueur();
     attaque = gamer->attaqueCc();
     int random = rand()%2;
     if (random == 0)
     {
         ennemi->attaqueCc();
     }
     else if (random ==2)
     {
         ennemi->attaqueF();
     }
     else
     {
         ennemi->attaquePr();
     }
     return win;
 }
 bool Game::attaqueF()
 {
     joueur *ennemi = new joueur();
     int random = rand()%2;
     attaque = gamer->attaqueF();
     if (random == 0)
     {
         ennemi->attaqueCc();
     }
     else if (random ==1)
     {
         ennemi->attaqueF();
     }
     else
     {
         ennemi->attaquePr();
     }
     return win;
 }
 bool Game::attaquePr()
 {
     joueur *ennemi = new joueur();
     attaque = gamer->attaquePr();
     int random = rand()%2;
     if (random == 0)
     {
         attaqueEnnemi = ennemi->attaqueCc();
     }
     else if (random ==1)
     {
         attaqueEnnemi = ennemi->attaqueF();
     }
     else
     {
         attaqueEnnemi = ennemi->attaquePr();
     }

     if (attaque > attaqueEnnemi)
     {
         win=true;
     }
     else
     {
         win=false;
     }
     return win;
 }
