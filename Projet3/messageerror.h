#ifndef MESSAGEERROR_H
#define MESSAGEERROR_H

#include <QWidget>

namespace Ui {
class MessageError;
}

class MessageError : public QWidget
{
    Q_OBJECT

public:
    explicit MessageError(QWidget *parent = 0);
    ~MessageError();

private slots:
    void on_pushButton_clicked();

private:
    Ui::MessageError *ui;
};

#endif // MESSAGEERROR_H
