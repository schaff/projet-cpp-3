#include "joueur.h"

joueur::joueur()
{
    Pv=100;
    dext=100;
    creveCoeur=10;
    fist=12;
    pettageRupteur=8;
}

joueur::getPv()
{
    return Pv;
}
joueur::getDext()
{
    return  dext;
}
void joueur::setPv(int pv){
    this->Pv = pv;
}
void joueur::setDext(int dext){
    this->dext = dext;
}

int joueur::attaqueCc()
{
    int coup = creveCoeur + dext / (Pv / 4);
    return coup;
}
int joueur::attaqueF()
{
    int coup = fist + dext / (Pv / 6);
    return coup;
}

int joueur::attaquePr()
{
    int coup = pettageRupteur + dext / (Pv / 2);
    return coup;
}
