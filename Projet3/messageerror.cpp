#include "messageerror.h"
#include "ui_messageerror.h"

MessageError::MessageError(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MessageError)
{
    ui->setupUi(this);
}

MessageError::~MessageError()
{
    delete ui;
}

void MessageError::on_pushButton_clicked()
{
    this->close();
}
