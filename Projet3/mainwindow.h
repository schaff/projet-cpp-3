#ifndef MAINWINDOW_H
#define MAINWINDOW_H


/**
 * \file mainwindow.h
 * \brief Fenetre jeu
 * \author Philippe.S
 * \version 0.1
 * \date 11/04/2017
 *
 * Fichier H qui controle la fenetre d'accueil mainwindow
 *
 */

#include <QMainWindow>
#include <string>
#include "sqlite.h"
#include <qtextedit.h>
#include <QMediaPlayer>
#include <QMediaPlaylist>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btnNewGame_clicked();

    void on_btnQuit_clicked();

    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
    Sqlite *query;
    QMediaPlayer *musique;
    QMediaPlaylist *list;
};

#endif // MAINWINDOW_H
