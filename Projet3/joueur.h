#ifndef JOUEUR_H
#define JOUEUR_H


/**
 * \file joueur.h
 * \brief Fenetre jeu
 * \author Philippe.S
 * \version 0.1
 * \date 11/04/2017
 *
 * Fichier H qui instensi la class joueur
 *
 */


class joueur
{
public:
    joueur();

    int getPv();
    void setPv(int pv);

    int getDext();
    void setDext(int dext);

    int attaqueCc();
    int attaqueF();
    int attaquePr();

private:
    int Pv;
    int dext;
    int creveCoeur;
    int fist;
    int pettageRupteur;
};

#endif // JOUEUR_H
