#include "sqlite.h"
#include "mainwindow.h"
#include <iostream>

Sqlite::Sqlite()
{
    dbmgr=QSqlDatabase::addDatabase("QSQLITE");
    dbmgr.setDatabaseName("database.db");

    if (dbmgr.open())
    {
        std::cout<<"Database opened"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR CONNEX DATABASE";
    }
}

void Sqlite::createDbTables()
{
    QSqlQuery query;
    query.prepare("CREATE TABLE IF NOT EXISTS gamer (name, option4, option5, option7, option12, option17, random12)");
    if (query.exec())
    {
        std::cout<<"Table gamer created"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR CREATE TABLE"<<query.lastError();
    }

    query.prepare("CREATE TABLE IF NOT EXISTS story (prelude, situation1, situation2, situation3, situation4, situation5, situation6, "
                  "situation7, situation8, situation9, situation10, situation11, situation12, situatuion13, situation14, situation15, "
                  "situation16, situation17, situation18, situation19, situation20, end1, end2, end3)");
    if(query.exec())
    {
        std::cout<<"Table stroy created"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR CREATE TABLE"<<query.lastError();
    }
}

void Sqlite::insertJoueur(QString name)
{
    QSqlQuery query;
    query.prepare("INSERT INTO gamer (name) VALUES (:param)");
    query.bindValue(":param", name);
    if (query.exec())
    {
        std::cout<<"Insert name gamer ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR INSERT GAMER"<<query.lastError();
    }
}

void Sqlite::selectJoueur()
{
    QSqlQuery query;
}

void Sqlite::insertHistoire()
{
    QSqlQuery query;
    query.prepare("INSERT INTO story (prelude, situation1, situation2, situation3, situation4, situation5, situation6, "
                  "situation7, situation8, situation9, situation10, situation11, situation12, situatuion13, situation14, situation15, "
                  "situation16, situation17, situation18, situation19, situation20, end1, end2, end3) VALUES ('\tBienvenue dans cette « aventure dont vous êtes le héros ». Dans n’importe quelle autre aventure du genre, on vous appellerait « Loup Solitaire », « Chasseur de Dragon » ou encore « Pourfendeur d’Orcs ». Ici vous n’êtes que Michel (même votre nom pue la défaite). Une personne lambda dans une ville lambda se contentant d’une vie lambda.\n"

                  "\tA certains moments, votre seul allié sera le hasard (il pourra aussi devenir votre pire ennemi). Par flemme narratologique et pour des raisons techniques, lors d’un lancé de dé, contentez-vous de vous rappeler que plus le chiffre obtenu sur le dé est élevé, plus vous avez des chances de vous en sortir sans trop d’encombres dans ladite situation.\n"

                  "\tPour les combats que vous devrez mener, retenez que certaines de vos actions ont des conséquences directes sur les statistiques de votre personnage. Ne prenez pas une décision à la légère car celle-ci pourrait vous faire gagner ou perdre un futur affrontement. Votre santé représente votre état physique et psychologique. Si elle tombe à zéro, vous ne mourrez pas forcément mais perdrez l’affrontement dans lequel vous vous trouvez (… jusque-là c’est plutôt logique). La Dextérité représente votre chance de toucher votre adversaire ainsi que la puissance des coups que vous lui donnerez. Plus votre héro se sent bien, plus il aura de chances que ces coups atteignent sa cible.\n"

                  "\tBonne « aventure » et bon courage… il vous en faudra pour affronter la vie\n', '\t9h33. Vous vous réveillez péniblement de votre soirée d’hier (le vin rouge à bien plus taper que prévu). Vous êtes encore tellement dans le brouillard que seul le hasard pourra déterminer votre petit déjeuner.\n', '\tSuite à ce frugal repas, vous allez en cours. Évidemment, vous êtes en retard (comme tous les jours depuis… votre naissance). Heureusement, vous croisez Lucas, qui est toujours là pour vous remonter le moral et vous faire sourire.\n"
                  "\t« Salut Michel ! T’as pas l’air d’aller bien. Ça va ?\n"
                  "\t- Dure soirée. Disons que le Rouge fait encore effet. \n\t- Ah tu sais, moi aussi j’étais aventurier autrefois, et puis j’ai pris une flèche dans l’genou\n', '\tArrivez dans la salle, votre professeur ne peut pas s’empêcher de vous faire remarquer que vous êtes en retard :\n"
                  "\t« Je pourrais savoir pourquoi vous êtes encore en retard, Michel ? »\n"
                  "\tIl vous faut une réponse efficace. Et il vous la faut vite.', '\t19h30. Lucas vous appelle. Vous décrochez votre téléphone à clapet (parce que vous êtes old-school) et vous vous contentez d’un grognement en guise de « allo ? ».\n\t «  Salut mec, c’est Lucas ! Tu viens au bar ? Y a une bonne partie d’la promo’, on s’éclate vraiment bien ! »\n\t Vous n’aimez pas vraiment votre promotion. \n\t« En plus y a Micheline qui est là ! » \n\tVous aimez Micheline, dans votre promotion. Oui, c’est cliché mais c’est la vérité. Un aventurier a toujours une promise, une demoiselle en détresse qu’il se doit d’aller secourir. Dans les contes de fées, cette femme fatale finie avec le héros, tout le monde est content et tout va pour le mieux dans le meilleur des mondes. Dans votre situation en particulier, elle n’est pas du tout en détresse, sait à peine que vous existez et ne sait probablement pas à quoi vous ressemblez… et en plus, elle a approximativement 27 « héros » dans votre style qui seraient prêts à être à ses pieds pour avoir un peu de son attention. \n\tLucas attend votre réponse. Il serait déçu que vous n’alliez pas à cette soirée. La seule question que vous vous posez est : en avez-vous réellement quelque chose à faire de ce que pense Lucas ?',"
                  "'\t20h47. Vous arrivez au bar. La musique est trop forte et pas à votre goût. Il y a du monde. Trop de monde. Vous vous dites que vous serez bien plus à l’aise quand vous aurez trouvé Lucas et qu’il aura payé la consommation qu’il vous « doit ». Vous le trouvez enfin et allez au bar avec lui. Il est déjà légèrement éméché (ça veut dire qu’il est rond comme une queue d’pelle), il a du mal à parler mais décide de vous payer des shooters parce que « faut"
                  "s’ambiancer ». Vous ne faites pas le difficile et acceptez. Un shot (-5% de Dextérité). Une bière (-2% de Dextérité). Un shot (-5% de Dextérité). Un Picon-bière (+3% de santé (parce que c’est la vie !)). Un shot (-5% de Dextérité). Un shot (-5% de Dextérité). Le bar se réchauffe en même temps que vous montez en température. L’endroit vous semble de plus en plus vivable. Les gens sont toujours aussi bruyants mais ça ne vous dérange plus. \n\tTout à coup vous la voyez. Là, elle descend les escaliers. Vous vous dites que c’est le moment ou jamais, que vu votre état, de toute façon, il ne peut rien se passer de grave. Oui mais voilà, vous n’avez jamais eu confiance en vous et ce même en étant totalement imbibé. Vous commencez à imaginer les pires scénarios : qu’elle pourrait se contenter de rire et partir, qu’elle pourrait vous gifler, qu’elle parlera à ses copines du « relou » qui est venue lui parler en bafouillant. Bref, vous vous cherchez des excuses',"
                  "'\tVous titubez, luttez pour passer entre les chaises, les bourrés et les sacs qui traînent par terre.\n"
                  "\tFaites un lancé de dé pour savoir comment vous vous en sortez dans la salle.', '\tVous êtes (enfin) face à elle. C’est le moment. Vous vous engagez dans une lutte intérieure pour essayer de construire des phrases grammaticalement correctes, ne pas passer pour un gros lourd, ne pas lui vomir dessus et ne pas vous évanouir.\n"
                  "\tToutes vos actions et votre discussion sera déterminer par cette lutte contre vous-même. Bon courage. Votre concentration extrême vous coute 10% du total de votre vie.', '\tVous êtes (encore) au bar. Lucas est enfermé aux toilettes depuis un bon moment. Manque de chance pour lui, il a oublié sa carte de crédit sur le comptoir. Vous décidez de commander une autre tournée (merci le sans-contact qui ne nécessite pas de code). La question qui se pose est : que commander ?', '\tLucas revient enfin des toilettes. Il vous prend dans ses bras en vous disant que vous avez très bien fait de recommander. Le fait qu’il ait sûrement vomi ne l’a apparemment pas fait dessaouler. Dommage, ça vous aurait arrangé.\n\t« Alors, quoi d’neuf ? vous demande-t-il."
                  "\t- Pas grand-chose. »\n"
                  "\tVous connaissez Lucas. Il ne se souviendra de rien d’ici demain, quoi que vous lui disiez. Autant économiser votre salive et ne pas lui dire ce que vous avez en tête.', '\t13h. Cette dernière bouteille de vin Rouge avant d’être allé vous coucher ne vous a vraiment pas fait du bien. Vous décidez de ne pas aller en cours (de toute façon, ils sont déjà tous finis). La question se posant maintenant étant : comment occuper le reste de votre palpitante journée ?', '\t13h02. Vous allumez votre ordinateur. Vous allez faire un tour sur les réseaux sociaux juste pour avoir le plaisir de vous rappeler pourquoi vous n’aimez pas les gens. Entre les photos de chats, les citations qui sont censées vous « motiver » et les blaireaux qui se contentent de poster des « trop dure la vie », « content », et autres « hum » totalement cryptiques et incompréhensible, vous vous dites que vous êtes très bien chez vous, loin de tout ça, seul.\n\tPour vous occuper, vous vous dites que l’écriture pourrait vous libérer. Mais qu’écrire ? Votre vie ? Elle vous ennuierait si vous aviez à la relire. Aucun intérêt. Un livre dont vous seriez le héros ? Mais quel genre de héros seriez-vous ? Un grand musclé qui sauve le monde ? Très peu crédible. Un héros à qui il n’arrive que des choses sans intérêt ? Probablement. Mais qui aurait envie de faire une aventure dans laquelle on incarne un quidam menant une vie parfaitement lambda… ?\n"
                  "\tVous pensez finalement à boire à nouveau en vous disant que ça ira mieux une fois que votre sang aura été remplacé par l’alcool.', '\t14h42. Vous êtes sur une place depuis approximativement 2 minutes et 32 secondes quand l’un de ces gentils super-héros des temps moderne vous approche en vous demandant : « s’cuse moi, t’as une minute ? ». Vous savez parfaitement que cette minute se transformera en 20 minutes et qu’il n’est pas là pour vos beaux yeux ou avoir du feu mais uniquement pour sauver les petits Africains qui mangent des galettes de terre matin, midi et soir. Évidemment,"
                  "vous vous souciez bien peu de ces enfants qui meurent chaque jour (ils sont loin, et même s’ils ne l’étaient pas, ça ne changerait rien, du moment que votre vin Rouge vous attend chez vous). Dans Duke Nukem, vous lui auriez fait un énorme doigt d’honneur. Dans Postal 2, vous lui auriez pisser dessus. Vous n’êtes dans aucun de ses univers. Qu’allez-vous lui répondre ?', '\t16h07. Sur le chemin du retour, vous vous souvenez que vous n’avez plus rien à fumer chez vous et qu’une bouteille de vin Rouge en plus n’a jamais fait de mal à personne. Vous vous dirigez donc vers le super marché le plus proche pour acheter deux bouteilles à bas prix (parce que vous cuiter au Champagne est au-dessus de vos moyens).', '\t23h. Vous êtes chez vous, ivre (une fois de plus). « Where is my mind » tourne en boucle dans votre casque. Vous vous sentez… réveillé. C’est à peu près tout ce que vous ressentez à ce moment précis. Vous fixez votre bouteille à moitié vide en réfléchissant à tout, à rien, à la vie, à Micheline, aux quelques jours qui se sont écoulés. \nSoudainement, vous entendez une voix. Vous n’y répondez pas car vous savez pertinemment que vous êtes seul et que cette voix se trouve dans votre tête (et on ne répond pas aux personnes imaginaires, quand on est normalement constitué). La voix insiste et continue à vous parler.\n"
                  "\t« Alors, ces derniers jours, t’en penses quoi ?', '\t23h. Vous êtes chez vous, sobre et pas défoncé (c’est suffisamment rare pour être souligné). « What a Wonderful World » tourne en boucle dans votre casque. Vous vous sentez vide. Sans intérêt. Vous fixez votre téléphone portable qui n’a pas vibré depuis des lustres à part pour vous rappeler que votre forfais a été renouvelé. Vous vous rendez compte que vous êtes seul et que vous le serez probablement pendant tout le reste de votre vie. Vous ne trouvez même pas la motivation de vous ouvrir une bouteille de vin Rouge.\n"
                  "\tSoudainement vous entendez quelqu’un se racler la gorge. Vous sursautez et vous vous retournez à toute vitesse pour voir qui a pu faire ce bruit. Un homme est assis sur votre lit. Il a les cheveux ébouriffés, trois poiles au menton et porte une veste en cuir rouge. Vous"
                  "n’avez pas souvenir d’avoir ouvert à qui que ce soit. Sans même vous dire bonjour, il commence à vous parler en ces termes :\n"
                  "\t« Alors ces derniers jours, t’en penses quoi ?', '\t9h12. Vous vous réveillé avec un mal de tête des enfers et êtes affalé sur votre table. Vous vous dites que vous vous êtes endormi ici. C’est la seule explication qui tienne la route. Vous repensez à la voix que vous avez entendu la veille. D’où venait-elle ? Y avait-il réellement une voix ? Y avait-il réellement quelqu’un ? Devenez-vous dingue ? Vous décidez de ne pas trop vous poser de questions d’aussi bon matin et préférez monter sur le toit de votre immeuble pour aller fumer une cigarette. Celle-ci à peine allumée, la voix se fait à nouveau entendre.\n"
                  "\t« Ben voilà, on va pouvoir causer, dit-il.\n"
                  "\t- Super. J’hallucine encore. - J’suis là, l’débile ».\n"
                  "\tUn homme est venu se placer juste à côté de vous. Il a exactement la même voix que celle que vous avez entendu hier. Un long silence s’installe durant lequel les volutes sont la seule et unique chose qui retienne votre attention à tous les deux. Puis il enchaîne :\n"
                  "\t« J’suis qui, d’après toi ?', '\tIl n’a pas l’air décidé à se taire et continue :\n"
                  "\t« J’ai la solution à peu près tous tes problèmes.\n"
                  "\t- Qui a dit que j’avais des problèmes ? \n\t- Tu t’fous d’moi ? Tout l’monde a des problèmes. ‘Fin, toutes les personnes intelligentes, du moins. Les autres se contentent de vivre, tranquillement. Maintenant la vraie question c’est : cette solution, tu la veux ou pas ? »\n"
                  "\tVous êtes pris au piège et savez qu’aucun tour de passe-passe ne pourra vous sortir de là.', '\tSuite à ce long monologue durant lequel vous vous êtes quand même gentiment fait défoncé, l’homme se décide enfin à vous proposer quelque chose :\n"
                  "\t« Voilà c’que j’te propose. T’as 2 choix… ‘fin non, un poil plus, mais fuck it, disons qu’t’as 2 choix parce que c’est moi qui parle depuis avant.»\n"
                  "\tL’homme vous tend un révolver et un lecteur MP3 (parce qu’il est old-school).\n"
                  "\t« Donc là, après m’être fait traiter comme une merde, j’ai l’choix entre un flingue et un lecteur MP3 des années 2000 ? Me foutre un pruneau ou m’rendre sourd avec Patrick Sébastien, c’est ça tes ‘solutions’ ? T’es du genre foireux, comme mec, j’me trompe ?\n"
                  "\t- En même temps, si t’arrêtais d’être con 3 minutes, la vie serait vachement plus simple. Mais en attendant je t’ouvre une porte de sortie, à toi d’choisir si tu veux la prendre ou si tu veux continuer à vivre ta p’tite vie ‘tranquille’. C’est de toute façon tellement plus simple de se complaire dans sa propre misère plutôt que d’essayer d’se bouger l’cul. Mais vas-y, pars, j’vais pas t’retenir et j’vais pas t’forcer à faire un truc que tu voudrais pas faire. Le MP3, c’est juste histoire de t’ambiancer une dernière fois si jamais tu décidais d’faire ‘le grand saut’. Mais comme dit avant, tu peux toujours partir si t’en as tellement envie. Et cette fois j’te retiendrai pas »',"
                  "'\tVous saisissez les objets. En jetant un œil au lecteur MP3, vous constatez qu’il ne contient que 3 chansons. Vous priez pour que ces 3 chansons ne soit pas des « tubes » de la compagnie créole, sinon, même votre mort aura été un échec critique.\n"
                  "\tVous devez choisir le dernier titre que vous écouterez de votre vie, le morceau que l’on jouera à votre enterrement s’il y avait des gens pour se soucier de celui-ci.', 'Vous regardez l’horizon, de la musique dans les oreilles, le revolver à la main. Vous jetez un œil à l’intérieur des différentes chambres et constatez qu’il y a 2 balles au lieu d’une habituellement. Vous entendez la voix de votre « compère » qui vous dit que vu l’état de votre vie, aider le destin ne peut pas vous faire de mal si c’est vraiment ce que vous voulez faire. La roulette russe sera votre salut. Vous collez l’arme contre votre tempe.\n"
                  "\tVous remettez votre destin au hasard. Faites un lancé de dé pour savoir ce qu’il adviendra de vous.', '\tVotre doigt presse la détente. A ce moment là, c’est comme si le temps s’était soudainement figé tout autour de vous. Comme on le raconte depuis la nuit des temps, vous voyez toute votre vie défiler devant vos yeux. Autant dire qu’il ne se passe rien de passionnant et que c’est d’un ennui sans nom. Les seules choses que vous voyez clairement sont des verres et des bouteilles à moitiés vide, des cuvettes de toilettes en gros plans, des moments durant lesquels vous vous fixez dans un miroir et, bien évidemment, Micheline, la femme dont vous rêvez depuis bien trop longtemps. \n\tTout ce que vous avez connu, tous ceux que vous avez connu, tout ce que vous avez fait, tout ce que vous aviez envie de faire, tout ça n’a plus d’importance. Tout s’est terminé une fraction de seconde après que vous ayez appuyé sur la gâchette. Certains appelleraient ça du gâchis ou de la lâcheté. D’autre y verrait le courage qu’il vous aura fallu pour en finir avec tout ceci. Dans tous les cas, vous ne vous souciez plus de rien, maintenant. Juste avant que la balle ne pénètre votre crane, vous vous dites que vous allez sûrement ruiner l’écouteur que vous avez dans l’oreille droite, mais il est trop tard pour le sauver.', '\tMême la mort ne veut pas de vous, aujourd’hui… et vous ne voulez plus d’elle non plus. Vous vous dite que ce click est une sorte de signe divin et vous décidez de rentrer chez vous, en sueur après être passé à 2 doigts d’une fin tragique. L’homme qui vous parlait jusqu’à présent n’est plus là. Vous ne savez pas s’il est parti ou s’il n’a jamais été présent, mais dans tous les cas, vous êtes parfaitement heureux de son absence. \n\tEn rentrant chez vous, vous constatez que vous avez reçu un message. C’est Lucas qui vous dit qu’il est au bar. Vous lui rappelez qu’il n’est même pas encore midi mais que vous auriez bien besoin d’un verre (ou de plusieurs) pour lui raconter ce que vous venez de traverser, qu’il soit sobre ou non.', '\t9h33. Vous vous réveillez péniblement. Vous vous demandez si ce que vous avez vécu ces derniers jours était réel ou non. Puis vous vous rappelez que de toute façon, ça n’a pas grande importance. Bref, vous décidez de vous faire un petit déjeuner et… et vous connaissez la suite de cette histoire puisque vous l’avez déjà vécu et, vu les choix que vous avez fait, vous risquez de revivre les mêmes situations encore et encore.')");
    if (query.exec())
    {
        std::cout<<"Insert story ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR INSERT STORY"<<query.lastError();
    }
}

void Sqlite::insertRandom12(int param)
{
    QSqlQuery query;
    QSqlQuery sql;
    sql.prepare("SELECT MAX(rowid) AS 'idMax' FROM gamer");
    int lastId;
    if (sql.exec())
    {
        sql.next();
        std::cout<<"OK SELECT MAX"<<std::endl;
        lastId=sql.value("idMax").toInt();
        std::cout<<lastId<<std::endl;
    }
    query.prepare("UPDATE gamer SET random12 = :param WHERE rowid= :value");
    query.bindValue(":param", param);
    query.bindValue(":value", lastId);
    if (query.exec())
    {
        std::cout<<"Update random12 ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR update random12"<<query.lastError();
    }
}

void Sqlite::insertSituation4(int param)
{
    QSqlQuery query;
    QSqlQuery sql;
    sql.prepare("SELECT MAX(rowid) AS 'idMax' FROM gamer");
    int lastId;
    if (sql.exec())
    {
        sql.next();
        std::cout<<"OK SELECT MAX"<<std::endl;
        lastId=sql.value("idMax").toInt();
        std::cout<<lastId<<std::endl;
    }
    query.prepare("UPDATE gamer SET option4 = :param WHERE rowid= :value");
    query.bindValue(":param", param);
    query.bindValue(":value", lastId);
    if (query.exec())
    {
        std::cout<<"Update situation4 ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR update SITUATION4"<<query.lastError();
    }
}

void Sqlite::insertSituation5(int param)
{
    QSqlQuery query;
    QSqlQuery sql;
    sql.prepare("SELECT MAX(rowid) AS 'idMax' FROM gamer");
    int lastId;
    if (sql.exec())
    {
        sql.next();
        std::cout<<"OK SELECT MAX"<<std::endl;
        lastId=sql.value("idMax").toInt();
        std::cout<<lastId<<std::endl;
    }
    query.prepare("UPDATE gamer SET option5 = :param WHERE rowid= :value");
    query.bindValue(":param", param);
    query.bindValue(":value", lastId);
    if (query.exec())
    {
        std::cout<<"Insert option5 ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR INSERT option5"<<query.lastError();
    }
}

void Sqlite::insertSituation7(int param)
{
    QSqlQuery query;
    QSqlQuery sql;
    sql.prepare("SELECT MAX(rowid) AS 'idMax' FROM gamer");
    int lastId;
    if (sql.exec())
    {
        sql.next();
        std::cout<<"OK SELECT MAX"<<std::endl;
        lastId=sql.value("idMax").toInt();
        std::cout<<lastId<<std::endl;
    }
    query.prepare("UPDATE gamer SET option7 = :param WHERE rowid= :value");
    query.bindValue(":param", param);
    query.bindValue(":value", lastId);
    if (query.exec())
    {
        std::cout<<"Insert option7 ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR INSERT option7"<<query.lastError();
    }
}

void Sqlite::insertSituation12(int param)
{
    QSqlQuery query;
    QSqlQuery sql;
    sql.prepare("SELECT MAX(rowid) AS 'idMax' FROM gamer");
    int lastId;
    if (sql.exec())
    {
        sql.next();
        std::cout<<"OK SELECT MAX"<<std::endl;
        lastId=sql.value("idMax").toInt();
        std::cout<<lastId<<std::endl;
    }
    query.prepare("UPDATE gamer SET option12 = :param WHERE rowid= :value");
    query.bindValue(":param", param);
    query.bindValue(":value", lastId);
    if (query.exec())
    {
        std::cout<<"Insert option12 ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR INSERT option12"<<query.lastError();
    }
}

void Sqlite::insertSituation17(int param)
{
    QSqlQuery query;
    QSqlQuery sql;
    sql.prepare("SELECT MAX(rowid) AS 'idMax' FROM gamer");
    int lastId;
    if (sql.exec())
    {
        sql.next();
        std::cout<<"OK SELECT MAX"<<std::endl;
        lastId=sql.value("idMax").toInt();
        std::cout<<lastId<<std::endl;
    }
    query.prepare("UPDATE gamer SET option17 = :param WHERE rowid= :value");
    query.bindValue(":param", param);
    query.bindValue(":value", lastId);
    if (query.exec())
    {
        std::cout<<"Insert option17 ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR INSERT option17"<<query.lastError();
    }
}

QString Sqlite::selectHistoire()
{
    QSqlQuery query;
    QString prelude;
    query.prepare("SELECT rowid, prelude FROM story");
    if(query.exec())
    {
        std::cout<<"Select prelude ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR SELECT PRELUDE"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("prelude").toString();
    return query.value("prelude").toString();
}

QString Sqlite::selectSituation1()
{
    QSqlQuery query;
    QString situation;
    query.prepare("SELECT rowid, situation1 FROM story");
    if(query.exec())
    {
        std::cout<<"Select situation ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR SELECT PRELUDE"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("situation1").toString();
    return query.value("situation1").toString();
}

QString Sqlite::selectSituation2()
{
    QSqlQuery query;
    QString situation;
    query.prepare("SELECT rowid, situation2 FROM story");
    if(query.exec())
    {
        std::cout<<"Select situation ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR SELECT situation2"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("situation2").toString();
    return query.value("situation2").toString();
}
QString Sqlite::selectSituation3()
{
    QSqlQuery query;
    QString situation;
    query.prepare("SELECT rowid, situation3 FROM story");
    if(query.exec())
    {
        std::cout<<"Select situation ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR SELECT situation3"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("situation3").toString();
    return query.value("situation3").toString();
}
QString Sqlite::selectSituation4()
{
    QSqlQuery query;
    QString situation;
    query.prepare("SELECT rowid, situation4 FROM story");
    if(query.exec())
    {
        std::cout<<"Select situation ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR SELECT situation4"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("situation4").toString();
    return query.value("situation4").toString();
}
QString Sqlite::selectSituation5()
{
    QSqlQuery query;
    QString situation;
    query.prepare("SELECT rowid, situation5 FROM story");
    if(query.exec())
    {
        std::cout<<"Select situation ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR SELECT situation5"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("situation5").toString();
    return query.value("situation5").toString();
}
QString Sqlite::selectSituation6()
{
    QSqlQuery query;
    QString situation;
    query.prepare("SELECT rowid, situation6 FROM story");
    if(query.exec())
    {
        std::cout<<"Select situation ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR SELECT situation6"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("situation6").toString();
    return query.value("situation6").toString();
}
QString Sqlite::selectSituation7()
{
    QSqlQuery query;
    QString situation;
    query.prepare("SELECT rowid, situation7 FROM story");
    if(query.exec())
    {
        std::cout<<"Select situation ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR SELECT situation7"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("situation7").toString();
    return query.value("situation7").toString();
}
QString Sqlite::selectSituation8()
{
    QSqlQuery query;
    QString situation;
    query.prepare("SELECT rowid, situation8 FROM story");
    if(query.exec())
    {
        std::cout<<"Select situation ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR SELECT situation8"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("situation8").toString();
    return query.value("situation8").toString();
}
QString Sqlite::selectSituation9()
{
    QSqlQuery query;
    QString situation;
    query.prepare("SELECT rowid, situation9 FROM story");
    if(query.exec())
    {
        std::cout<<"Select situation ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR SELECT situation9"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("situation9").toString();
    return query.value("situation9").toString();
}
QString Sqlite::selectSituation10()
{
    QSqlQuery query;
    QString situation;
    query.prepare("SELECT rowid, situation10 FROM story");
    if(query.exec())
    {
        std::cout<<"Select situation ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR SELECT situation10"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("situation10").toString();
    return query.value("situation10").toString();
}
QString Sqlite::selectSituation11(){
    QSqlQuery query;
    QString situation;
    query.prepare("SELECT rowid, situation11 FROM story");
    if(query.exec())
    {
        std::cout<<"Select situation ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR SELECT situation11"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("situation11").toString();
    return query.value("situation11").toString();
}
QString Sqlite::selectSituation12(){
    QSqlQuery query;
    QString situation;
    query.prepare("SELECT rowid, situation12 FROM story");
    if(query.exec())
    {
        std::cout<<"Select situation ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR SELECT situation12"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("situation12").toString();
    return query.value("situation12").toString();
}
QString Sqlite::selectSituation13(){
    QSqlQuery query;
    QString situation;
    query.prepare("SELECT rowid, situation13 FROM story");
    if(query.exec())
    {
        std::cout<<"Select situation ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR SELECT situation13"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("situation13").toString();
    return query.value("situation13").toString();
}
QString Sqlite::selectSituation14(){
    QSqlQuery query;
    QString situation;
    query.prepare("SELECT rowid, situation14 FROM story");
    if(query.exec())
    {
        std::cout<<"Select situation ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR SELECT situation14"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("situation14").toString();
    return query.value("situation14").toString();
}
QString Sqlite::selectSituation15(){
    QSqlQuery query;
    QString situation;
    query.prepare("SELECT rowid, situation15 FROM story");
    if(query.exec())
    {
        std::cout<<"Select situation ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR SELECT situation15"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("situation15").toString();
    return query.value("situation15").toString();
}
QString Sqlite::selectSituation16(){
    QSqlQuery query;
    QString situation;
    query.prepare("SELECT rowid, situation16 FROM story");
    if(query.exec())
    {
        std::cout<<"Select situation ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR SELECT situation16"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("situation16").toString();
    return query.value("situation16").toString();
}
QString Sqlite::selectSituation17(){
    QSqlQuery query;
    QString situation;
    query.prepare("SELECT rowid, situation17 FROM story");
    if(query.exec())
    {
        std::cout<<"Select situation ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR SELECT situation17"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("situation17").toString();
    return query.value("situation17").toString();
}
QString Sqlite::selectSituation18(){
    QSqlQuery query;
    QString situation;
    query.prepare("SELECT rowid, situation18 FROM story");
    if(query.exec())
    {
        std::cout<<"Select situation ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR SELECT situation18"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("situation18").toString();
    return query.value("situation18").toString();
}
QString Sqlite::selectSituation19(){
    QSqlQuery query;
    QString situation;
    query.prepare("SELECT rowid, situation19 FROM story");
    if(query.exec())
    {
        std::cout<<"Select situation ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR SELECT situation19"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("situation19").toString();
    return query.value("situation19").toString();
}
QString Sqlite::selectSituation20(){
    QSqlQuery query;
    QString situation;
    query.prepare("SELECT rowid, situation20 FROM story");
    if(query.exec())
    {
        std::cout<<"Select situation ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR SELECT situation20"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("situation20").toString();
    return query.value("situation20").toString();
}
QString Sqlite::selectEnd1(){
    QSqlQuery query;
    QString situation;
    query.prepare("SELECT rowid, end1 FROM story");
    if(query.exec())
    {
        std::cout<<"Select end1 ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR SELECT end1"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("end1").toString();
    return query.value("end1").toString();
}
QString Sqlite::selectEnd2(){
    QSqlQuery query;
    QString situation;
    query.prepare("SELECT rowid, end2 FROM story");
    if(query.exec())
    {
        std::cout<<"Select end2 ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR SELECT end2"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("end2").toString();
    return query.value("end2").toString();
}
QString Sqlite::selectEnd3(){
    QSqlQuery query;
    QString situation;
    query.prepare("SELECT rowid, end3 FROM story");
    if(query.exec())
    {
        std::cout<<"Select end3 ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR SELECT end3"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("end3").toString();
    return query.value("end3").toString();
}

int Sqlite::selectOption4()
{
    QSqlQuery query;
    QSqlQuery sql;
    sql.prepare("SELECT MAX(rowid) AS 'idMax' FROM gamer");
    int lastId;
    if (sql.exec())
    {
        sql.next();
        std::cout<<"OK SELECT MAX"<<std::endl;
        lastId=sql.value("idMax").toInt();
        std::cout<<lastId<<std::endl;
    }
    query.prepare("SELECT rowid, option4 FROM gamer WHERE rowid= :value");
    query.bindValue(":value", lastId);
    if (query.exec())
    {
        std::cout<<"Select option4 ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR select otpion4"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("option4").toInt();
    return query.value("option4").toInt();
}
int Sqlite::selectOption5()
{
    QSqlQuery query;
    QSqlQuery sql;
    sql.prepare("SELECT MAX(rowid) AS 'idMax' FROM gamer");
    int lastId;
    if (sql.exec())
    {
        sql.next();
        std::cout<<"OK SELECT MAX"<<std::endl;
        lastId=sql.value("idMax").toInt();
        std::cout<<lastId<<std::endl;
    }
    query.prepare("SELECT rowid, option5 FROM gamer WHERE rowid= :value");
    query.bindValue(":value", lastId);
    if (query.exec())
    {
        std::cout<<"Select option5 ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR select otpion5"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("option5").toInt();
    return query.value("option5").toInt();
}
int Sqlite::selectOption7()
{
    QSqlQuery query;
    QSqlQuery sql;
    sql.prepare("SELECT MAX(rowid) AS 'idMax' FROM gamer");
    int lastId;
    if (sql.exec())
    {
        sql.next();
        std::cout<<"OK SELECT MAX"<<std::endl;
        lastId=sql.value("idMax").toInt();
        std::cout<<lastId<<std::endl;
    }
    query.prepare("SELECT rowid, option7 FROM gamer WHERE rowid= :value");
    query.bindValue(":value", lastId);
    if (query.exec())
    {
        std::cout<<"Select option7 ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR select otpion7"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("option7").toInt();
    return query.value("option7").toInt();
}
int Sqlite::selectOption12()
{
    QSqlQuery query;
    QSqlQuery sql;
    sql.prepare("SELECT MAX(rowid) AS 'idMax' FROM gamer");
    int lastId;
    if (sql.exec())
    {
        sql.next();
        std::cout<<"OK SELECT MAX"<<std::endl;
        lastId=sql.value("idMax").toInt();
        std::cout<<lastId<<std::endl;
    }
    query.prepare("SELECT rowid, option12 FROM gamer WHERE rowid= :value");
    query.bindValue(":value", lastId);
    if (query.exec())
    {
        std::cout<<"Select option12 ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR select otpion12"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("option12").toInt();
    return query.value("option12").toInt();
}
int Sqlite::selectOption17()
{
    QSqlQuery query;
    QSqlQuery sql;
    sql.prepare("SELECT MAX(rowid) AS 'idMax' FROM gamer");
    int lastId;
    if (sql.exec())
    {
        sql.next();
        std::cout<<"OK SELECT MAX"<<std::endl;
        lastId=sql.value("idMax").toInt();
        std::cout<<lastId<<std::endl;
    }
    query.prepare("SELECT rowid, option17 FROM gamer WHERE rowid= :value");
    query.bindValue(":value", lastId);
    if (query.exec())
    {
        std::cout<<"Select option17 ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR select otpion17"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("option17").toInt();
    return query.value("option17").toInt();
}
int Sqlite::selectRandom12()
{
    QSqlQuery query;
    QSqlQuery sql;
    sql.prepare("SELECT MAX(rowid) AS 'idMax' FROM gamer");
    int lastId;
    if (sql.exec())
    {
        sql.next();
        std::cout<<"OK SELECT MAX"<<std::endl;
        lastId=sql.value("idMax").toInt();
        std::cout<<lastId<<std::endl;
    }
    query.prepare("SELECT rowid, random12 FROM gamer WHERE rowid= :value");
    query.bindValue(":value", lastId);
    if (query.exec())
    {
        std::cout<<"Select random12 ok"<<std::endl;
    }
    else
    {
        qDebug()<<"ERROR select random12"<<query.lastError();
    }
    query.next();
    qDebug()<<query.value("random12").toInt();
    return query.value("random12").toInt();
}
