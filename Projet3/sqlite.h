#ifndef SQLITE_H
#define SQLITE_H

/**
 * \file sqlite.h
 * \brief Fenetre jeu
 * \author Philippe.S
 * \version 0.1
 * \date 11/04/2017
 *
 * Fichier H qui instensi la class de sqlite (La BDD)
 *
 */

#include <QSqlDatabase>
#include <QSqlError>
#include <qsqlquery.h>
#include <QDebug>

class mainwindow;

class Sqlite
{
public:
    Sqlite();
    void createDbTables();
    void insertJoueur(QString name);
    void selectJoueur();
    void insertSituation4(int param);
    void insertSituation5(int param);
    void insertSituation7(int param);
    void insertSituation12(int param);
    void insertSituation17(int param);
    void insertRandom12(int param);
    void insertHistoire();

    QString selectHistoire();
    QString selectSituation1();
    QString selectSituation2();
    QString selectSituation3();
    QString selectSituation4();
    QString selectSituation5();
    QString selectSituation6();
    QString selectSituation7();
    QString selectSituation8();
    QString selectSituation9();
    QString selectSituation10();
    QString selectSituation11();
    QString selectSituation12();
    QString selectSituation13();
    QString selectSituation14();
    QString selectSituation15();
    QString selectSituation16();
    QString selectSituation17();
    QString selectSituation18();
    QString selectSituation19();
    QString selectSituation20();
    QString selectEnd1();
    QString selectEnd2();
    QString selectEnd3();

    int selectOption4();
    int selectOption5();
    int selectOption7();
    int selectOption12();
    int selectOption17();
    int selectRandom12();

    void selectPrelude();
private:
    QSqlDatabase dbmgr;
};

#endif // SQLITE_H
