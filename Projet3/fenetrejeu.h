#ifndef FENETREJEU_H
#define FENETREJEU_H
/**
 * \file fenetrejeu.h
 * \brief Fenetre jeu
 * \author Philippe.S
 * \version 0.1
 * \date 11/04/2017
 *
 * Fichier H qui controle la fenetre de fin
 *
 */

#include <QMainWindow>
#include "sqlite.h"
#include <QWidget>
#include <iostream>
#include <time.h>
#include <QScrollBar>
#include <qradiobutton.h>
#include "end.h"
#include <QMediaPlayer>
#include <QMediaPlaylist>



class Game;

namespace Ui {
class FenetreJeu;
}

class FenetreJeu : public QMainWindow
{
    Q_OBJECT

public:
    explicit FenetreJeu(QMainWindow *parent = 0);
    FenetreJeu (Sqlite *query);
    ~FenetreJeu();

    int getCompteur();
    void setCompteur(int compteur);

    int getRandom();

    void scrollMax();

    void checked();

    void end();

private slots:
    void on_btnRandom_clicked();

    void on_btnOk_clicked();

    void on_RbChoose1_clicked();

    void on_RbChoose2_clicked();

    void on_RbChoose3_clicked();

    void on_pushButton_clicked();

private:
    Ui::FenetreJeu *ui;
    Sqlite *query;
    int compteur;
    int random;
    Game *jeu;
    bool win;
    int controle;
    END *fin;
    QMediaPlayer *sik;
    QMediaPlaylist *lyst;
    QMediaPlayer *music;
    QMediaPlaylist *li;
};

#endif // FENETREJEU_H
