#include "fenetrepseudo.h"
#include "ui_fenetrepseudo.h"
#include "fenetrejeu.h"
#include "mainwindow.h"
#include <qmainwindow.h>

#include "fenetrejeu.h"

fenetrePseudo::fenetrePseudo(QMainWindow *parent) :
    QMainWindow(parent),
    ui(new Ui::fenetrePseudo)
{
    ui->setupUi(this);
}

fenetrePseudo::fenetrePseudo(Sqlite *query) :
    ui(new Ui::fenetrePseudo)
{
    ui->setupUi(this);
    this->query = query;
    ui->label_2->setVisible(false);
}

fenetrePseudo::~fenetrePseudo()
{
    delete ui;
    delete query;
}

void fenetrePseudo::on_btnStartGame_clicked()
{
    name = ui->namePerso->toPlainText();
    if (name.isEmpty() || name==" "){
        ui->label_2->setVisible(true);
        ui->namePerso->setText("");
    }
    else
    {
        query->insertJoueur(name);
        this->close();
        FenetreJeu *fenetre2 = new FenetreJeu(query);
        fenetre2->show();
    }
}
