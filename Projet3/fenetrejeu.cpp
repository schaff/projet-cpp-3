#include "fenetrejeu.h"
#include "ui_fenetrejeu.h"
#include "game.h"

FenetreJeu::FenetreJeu(QMainWindow *parent) :
    QMainWindow(parent),
    ui(new Ui::FenetreJeu)
{
    ui->setupUi(this);
}

FenetreJeu::FenetreJeu( Sqlite *query):
    ui(new Ui::FenetreJeu)
{
    ui->setupUi(this);
    this->query = query;
    jeu = new Game(this);
    ui->textEdit->setText(query->selectHistoire());
    ui->RbChoose1->setVisible(false);
    ui->RbChoose2->setVisible(false);
    ui->RbChoose3->setVisible(false);
    ui->PbPv->setValue(jeu->gamer->getPv());
    ui->PbPv->setMaximum(100);
    ui->PbDext->setValue(jeu->gamer->getDext());
    ui->PbDext->setMaximum(100);
    ui->LChoose->setText("Cliquez sur poursuivre l'histoire pour continuer");
    compteur=0;
    srand(time(NULL)); 
    win=false;
    controle=0;
    fin=new END();

    //musique du jeu
    sik = new QMediaPlayer();
    lyst = new QMediaPlaylist();

    lyst->addMedia(QUrl("Instru.mp3"));
    lyst->setPlaybackMode(QMediaPlaylist::Loop);

    sik->setMedia(lyst);
    this->sik->setVolume(0);

    sik->play();
}

FenetreJeu::~FenetreJeu()
{
    delete ui;
    delete jeu;
    delete query;
}

void FenetreJeu::scrollMax()
{
    QScrollBar *sb = ui->textEdit->verticalScrollBar();
    sb->setValue(sb->maximum());
}

void FenetreJeu::on_btnRandom_clicked()
{
    random=rand()%6+1;
    std::cout<<random<<std::endl;
    if (getCompteur()==1)
    {
        jeu->situation1();
        if (random == 1 || random==2)
        {
            ui->LChoose->setText("Vous perdez 5 point de vie");
            ui->PbPv->setValue(jeu->gamer->getPv());
        }
        else if (random == 3 || random==4)
        {
            ui->LChoose->setText("Vous perdez 3 point de vie et 5 point de dextérité");
            ui->PbPv->setValue(jeu->gamer->getPv());
            ui->PbDext->setValue(jeu->gamer->getDext());
        }
        else if (random == 5 || random ==6)
        {
            ui->LChoose->setText("Vous gagné 3 point de dextérité");
            ui->PbDext->setMaximum(103);
            ui->PbDext->setValue(jeu->gamer->getDext());
        }
        ui->btnOk->setVisible(true);
    }
    if (getCompteur()==6)
    {
        jeu->situation6();
        if (random == 1 || random==2 || random==3)
        {
            ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\t Vous vous prenez un coup de coude, une bière sur le pantalon et une claque derrière la tête de la part d’un inconnu pour aucune raison apparente. Vous perdez 10% du total de votre vie… et un peu de votre dignité, mais vous n’êtes plus à ça près. ");
            ui->LChoose->setText("Vous perdez le combat");
            this->scrollMax();
            ui->PbPv->setValue(jeu->gamer->getPv());
        }
        else if (random == 4 || random == 5 || random ==6)
        {
            ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\t Avec la grâce du félin, vous vous faufilez entre tous les obstacles que vous rencontrez. Vous vous faites des films dans votre tête en vous disant que vous êtes le nouveau Houdini pour avoir réussi à tout esquiver de la sorte… En réalité, vous n’avez rien fait d’autre que marcher 5 mètres");
            ui->LChoose->setText("Vous gagnez le combat");
            this->scrollMax();
        }
        ui->btnOk->setVisible(true);
    }
    if (getCompteur() == 20 || getCompteur()==22)
    {
        win = jeu->rouletteRusse();
        ui->btnRandom->setVisible(false);
        if(win==true)
        {
            setCompteur(21);
        }
        else
        {
            setCompteur(20);
        }
        ui->btnOk->setVisible(true);
    }
}
void FenetreJeu::on_btnOk_clicked()
{
    switch (getCompteur())
    {
    case 0:
         ui->textEdit->setText(ui->textEdit->toPlainText() +"\t"+"\n"+"\n"+ query->selectSituation1());
         ui->LChoose->setText("Faite un lancé de dée pour définir votre petit déjeuné");
         ui->btnOk->setVisible(false);
         setCompteur(1);
         this->scrollMax();
         checked();
        break;
    case 1:
        ui->textEdit->setText(ui->textEdit->toPlainText() +"\n"+"\n"+ query->selectSituation2());
        ui->LChoose->setText("Que répondez vous ?");
        ui->RbChoose1->setText("Haha, Excellent");
        ui->RbChoose2->setText("TA GUEULE AVEC TES RÉFÉRENCES DE MERDE DÈS L’MATIN ! ");
        ui->RbChoose3->setText("ne rien dire");
        ui->RbChoose1->setVisible(true);
        ui->RbChoose2->setVisible(true);
        ui->RbChoose3->setVisible(true);
        ui->btnOk->setVisible(false);
        setCompteur(2);
        this->scrollMax();
        checked();
        break;
    case 2:
        ui->textEdit->setText(ui->textEdit->toPlainText() +"\n"+"\n"+  query->selectSituation3());
        ui->LChoose->setText("Que répondez vous ?");
        ui->RbChoose1->setText(" « Excusez-moi, c’est mon réveil, il a pas sonné. » Classique mais ça passe (presque) toujours. ");
        ui->RbChoose2->setText(" « S’cusez, j’avais la flemme de venir plus tôt ». Sur un malentendu, ça peut passer. ");
        ui->RbChoose3->setText("ne rien dire et vous asseoir. ");
        setCompteur(3);
        ui->btnOk->setVisible(false);
        this->scrollMax();
        checked();
        break;
    case 3:
        ui->textEdit->setText(ui->textEdit->toPlainText() +"\n"+"\n"+ query->selectSituation4());
        ui->LChoose->setText("Que répondez vous ?");
        ui->RbChoose1->setText("«Nah, c’est mort mec, faut qu’je bosse». Vous n’allez pas bosser. Vous allez dormir. Ou boire du Rouge… ou les deux.[Aller à 10]");
        ui->RbChoose2->setText("«T’es lourd mec, j’avais prévu des trucs, ce soir». C’est totalement faux mais Lucas n’est pas censé le savoir. «Bon ok, j’saute dans mes pompes et j’arrive».");
        ui->RbChoose3->setText("ne rien dire et raccrocher[Aller à 10]");
        setCompteur(4);
        this->scrollMax();
        ui->btnOk->setVisible(false);
        checked();
        break;
    case 4:
        ui->textEdit->setText(ui->textEdit->toPlainText() +"\n"+"\n"+ query->selectSituation5());
        ui->RbChoose3->setVisible(false);
        ui->RbChoose1->setText("aller la voir.");
        ui->RbChoose2->setText("ne rien faire [Aller à 8]");
        setCompteur(5);
        this->scrollMax();
        ui->btnOk->setVisible(false);
        jeu->situation5();
        checked();
        break;
    case 5:
        ui->textEdit->setText(ui->textEdit->toPlainText() +"\n"+"\n"+ query->selectSituation6());
        this->scrollMax();
        ui->LChoose->setText("");
        ui->PbPv->setValue(jeu->gamer->getPv());
        ui->PbPv->setValue(jeu->gamer->getDext());
        ui->RbChoose1->setVisible(false);
        ui->RbChoose2->setVisible(false);
        setCompteur(6);
        ui->btnOk->setVisible(false);
        checked();
        break;
    case 6:
        ui->textEdit->setText(ui->textEdit->toPlainText() +"\n"+"\n"+ query->selectSituation7());
        jeu->situation7();
        ui->PbPv->setValue(jeu->gamer->getPv());
        ui->LChoose->setText("COMBAT : Choisissez une attaque ! ");
        ui->RbChoose1->setText("Attaque : CreveCoeur");
        ui->RbChoose2->setText("Attaque : pettage rupteur");
        ui->RbChoose3->setText("Attaque : fist");
        ui->RbChoose1->setVisible(true);
        ui->RbChoose2->setVisible(true);
        ui->RbChoose3->setVisible(true);
        setCompteur(7);
        this->scrollMax();
        ui->btnOk->setVisible(false);
        checked();
        break;
    case 7:
        ui->textEdit->setText(ui->textEdit->toPlainText() +"\n"+"\n"+ query->selectSituation8());
        ui->PbPv->setValue(jeu->gamer->getPv());
        setCompteur(8);
        ui->RbChoose3->setVisible(false);
        ui->RbChoose1->setText("commander une pinte de bière");
        ui->RbChoose2->setText("commander une boisson sans alcool ");
        this->scrollMax();
        ui->btnOk->setVisible(false);
        checked();
        break;
    case 8:
        ui->textEdit->setText(ui->textEdit->toPlainText() +"\n"+"\n"+ query->selectSituation9());
        setCompteur(9);
        ui->RbChoose2->setVisible(false);
        ui->RbChoose1->setVisible(false);
        ui->LChoose->setText("cliquez sur Poursuivez l'histoire");
        this->scrollMax();
        checked();
        break;
    case 9:
        ui->textEdit->setText(ui->textEdit->toPlainText() +"\n"+"\n"+ query->selectSituation10());
        this->scrollMax();
        ui->RbChoose3->setVisible(false);
        ui->RbChoose2->setVisible(true);
        ui->RbChoose1->setVisible(true);
        ui->RbChoose1->setText("aller vous promener en ville [Aller à 12]");
        ui->RbChoose2->setText("ne rien faire chez vous");
        setCompteur(10);
        ui->btnOk->setVisible(false);
        checked();
        break;
    case 10:
        ui->textEdit->setText(ui->textEdit->toPlainText() +"\n"+"\n"+ query->selectSituation11());
        setCompteur(11);
        this->scrollMax();
        ui->RbChoose1->setText("commencerà boire. [Aller à 14]");
        ui->RbChoose2->setText("ne rien faire. [Aller à 15]");
        ui->btnOk->setVisible(false);
        checked();
        break;
    case 11:
        ui->textEdit->setText(ui->textEdit->toPlainText() +"\n"+"\n"+ query->selectSituation12());
        setCompteur(12);
        this->scrollMax();
        controle=0;
        ui->RbChoose3->setVisible(true);
        ui->RbChoose1->setText("Oui, bien sûr ");
        ui->RbChoose2->setText("Vous partez dans une joute verbale avec le jeune homme aux dreadlocks males odorantes pour essayer de lui expliquer que vous n’êtes pas intéressé. Le fait qu’il soit un L raté et un hippie (on appelle ça « un pléonasme ») vous donne confiance en vous. Vous gagnez 20% de Dextérité. ");
        ui->RbChoose3->setText(" Ne rien dire et partir. [Aller à 13] ");
        ui->btnOk->setVisible(false);
        checked();
        break;
    case 12:
        ui->textEdit->setText(ui->textEdit->toPlainText() +"\n"+"\n"+ query->selectSituation13());
        setCompteur(13);
        this->scrollMax();
        ui->LChoose->setText("Cliquez sur poursuivez l'histoire");
        checked();
        break;
    case 13:
        ui->textEdit->setText(ui->textEdit->toPlainText() +"\n"+"\n"+ query->selectSituation14());
        setCompteur(14);
        this->scrollMax();
        ui->RbChoose1->setText("J'ai connu pire");
        ui->RbChoose2->setText("PLutôt... à chier");
        ui->RbChoose3->setText("Ne rien dire et continuer à fixer votre bouteille");
        ui->btnOk->setVisible(false);
        checked();
        break;
    case 14:
        ui->textEdit->setText(ui->textEdit->toPlainText() +"\n"+"\n"+ query->selectSituation15());
        setCompteur(15);
        this->scrollMax();
        ui->RbChoose1->setText(" - Non mais What. The. Fuck, sérieusement ?! Premièrement : t’es qui ? Deuxièmement : qu’est c’que ça pourrait bien t’foutre ? Et troisièmement : comment t’es rentré chez moi, bordel ? »");
        ui->RbChoose2->setText(" - Euh… bonjour ? »");
        ui->RbChoose3->setText("Ne rien dire");
        ui->btnOk->setVisible(false);
        checked();
        break;
    case 15:
        ui->textEdit->setText(ui->textEdit->toPlainText() +"\n"+"\n"+ query->selectSituation16());
        setCompteur(16);
        this->scrollMax();
        ui->RbChoose1->setText("- Aucune idée. Et en plus j’m’en balance complètement");
        ui->RbChoose2->setText("- Avec des énigmes pareilles, j’aurais dit… Père Fouras.");
        ui->RbChoose3->setText("Ne rien dire");
        ui->btnOk->setVisible(false);
        checked();
        break;
    case 16:
        ui->textEdit->setText(ui->textEdit->toPlainText() +"\n"+"\n"+ query->selectSituation17());
        setCompteur(17);
        this->scrollMax();
        ui->RbChoose1->setText("« Ok, balance les explications à ‘pourquoi la vie ?’");
        ui->RbChoose2->setText("« C’est quoi l’embrouille ?");
        ui->RbChoose3->setText("Partir");
        ui->btnOk->setVisible(false);
        checked();
        break;
    case 17:
        ui->textEdit->setText(ui->textEdit->toPlainText() +"\n"+"\n"+ query->selectSituation18());
        setCompteur(18);
        this->scrollMax();
        ui->RbChoose1->setText("Prendre l'arme et l'iPod");
        ui->RbChoose2->setText("Partir. [Aller à 21]");
        ui->RbChoose3->setVisible(false);
        ui->btnOk->setVisible(false);
        checked();
        break;
    case 18:
        ui->textEdit->setText(ui->textEdit->toPlainText() +"\n"+"\n"+ query->selectSituation19());
        setCompteur(19);
        this->scrollMax();
        ui->RbChoose3->setVisible(true);
        ui->RbChoose1->setText("Kid Cudi - The Maniac");
        ui->RbChoose2->setText("L'Orange - Born Loser");
        ui->RbChoose3->setText("System of a Down - Chop Suey ! ");
        ui->btnOk->setVisible(false);
        checked();
        break;
    case 19:
        ui->textEdit->setText(ui->textEdit->toPlainText() +"\n"+"\n"+ query->selectSituation20());
        setCompteur(20);
        ui->btnRandom->setText("Appuyer sur la détente !");
        ui->RbChoose1->setVisible(false);
        ui->RbChoose2->setVisible(false);
        ui->RbChoose3->setVisible(false);
        ui->btnOk->setVisible(false);
        this->scrollMax();
        checked();
        break;
    case 20:
        ui->textEdit->setText(ui->textEdit->toPlainText() +"\n"+"\n"+ query->selectEnd1());
        setCompteur(21);
        ui->btnOk->setVisible(true);
        ui->btnOk->setText("END");
        this->scrollMax();
        checked();
        break;
    case 21:
        setCompteur(22);
        ui->btnOk->setVisible(false);
        ui->RbChoose1->setText("Rejouer à la roulette russe");
        ui->RbChoose2->setText("Partir");
        this->scrollMax();
        checked();
        break;
    case 22:
        ui->textEdit->setText(ui->textEdit->toPlainText() +"\n"+"\n"+ query->selectEnd3());
        setCompteur(23);
        ui->RbChoose1->setVisible(false);
        ui->RbChoose2->setVisible(false);
        ui->btnOk->setVisible(true);
        this->scrollMax();
        checked();
        break;
    case 23:
        this->close();
        fin->show();
        break;

    }
}

int FenetreJeu::getCompteur()
{
    return compteur;
}
void FenetreJeu::setCompteur(int compteur)
{
    this->compteur = compteur;
}
int FenetreJeu::getRandom()
{
    return random;
}

void FenetreJeu::on_RbChoose1_clicked()
{
    switch (getCompteur()){
    case 2:
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tJe savais qu’elle te ferait plaisir, celle-là ! » ");
        this->scrollMax();
        ui->btnOk->setVisible(true);
        break;
    case 3:
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\t« Votre réveil à tendance à vous faire souvent défaut, j’ai l’impression. Vous devriez peut-être songer à en changer ». Cette tentative d’humour ne fait absolument pas mouche et créé juste un lourd silence dans la salle. ");
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tVous passez une journée en enfer. Vous avez chaud, froid, êtes fatigué, en avez marre de tout (surtout des cours) et de tout le monde. Vous ne pensez qu’à une chose : rentrer chez vous.  ");
        this->scrollMax();
        ui->btnOk->setVisible(true);
        break;
    case 4:
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\t« Bon… ben c’est pas grave ». Votre camarade est sur la bonne pente pour devenir une victime de compétition.  ");
        query->insertSituation4(1);
        setCompteur(9);
        this->scrollMax();
        ui->btnOk->setVisible(true);
        break;
    case 5:
        ui->btnOk->setVisible(true);
        break;
    case 7:
        if (controle==0)
        {
            ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tVous lancez l'attaque crèveCoeur");
            win=jeu->attaqueCc();
            this->scrollMax();
            if (win==true)
            {
                ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tC’est fait, vous êtes arrivé jusqu’à elle, ne vous êtes pas étalé et n’avez pas vomi. Il ne vous reste plus qu’à trouver les bons mots !");
                ui->LChoose->setText("Vous gagnez le combat");
                this->scrollMax();
                ui->RbChoose1->setText("« Écoute, j’vais être direct, j’suis grave bourré et tu m’plais carrément.");
                ui->RbChoose2->setText("« T’as d’beaux yeux, tu sais ? ». Vous êtes bourré… trop bourré, peut-être. ");
                ui->RbChoose3->setText("« Euh… salut. Ça t’dirait qu’on aille fumer dehors ? On s’entend plus parler, ici.");
                controle=1;
            }
            else if (win == false)
            {
                ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tVotre concentration ne vous aura décidément pas aidé. Vous avez à cœur de lui citer un passage de « Roméo et Juliette » en anglais. Mais les seuls et uniques sons qui sortent de votre bouche sont parfaitement incompréhensibles pour le commun des mortels et ressemble approximativement à « qmsdkcjvnbsospanfkgùq ». Elle est foudroyée par la peur, vous dit « tu devrais boire un verre d’eau, ça t’ferait sûrement du bien » avec un sourire gêné et se dirige vers la sortie du bar. Vous vous dites que ça aurait pu être pire. Vous êtes clairement trop bourré, ça n’aurait pas pu être pire.");
                ui->LChoose->setText("Vous perdez le combat");
                this->scrollMax();
            }
        }
        else if (controle==1)
        {
            ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\t - Ah ouais. Effectivement, t’es plutôt atteint ! » Par miracle, elle sait gérer les mecs bourrés et à l’air de ne pas l’avoir mal pris ! Elle se contente de vous faire la bise et de sortir du bar. Vous gagnez 15% de vie (c’est ce qu’on appelle « un bisou magique »). ");
            jeu->situation7_1();
            this->scrollMax();
        }
        ui->btnOk->setVisible(true);
        break;
    case 8:
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\t« vous vous enfoncez encore un peu plus dans les abymes de l’alcoolémie. Vous perdez 10% de vie.");
        jeu->situation8_1();
        ui->PbPv->setValue(jeu->gamer->getPv());
        this->scrollMax();
        ui->btnOk->setVisible(true);
        break;
    case 10:
        setCompteur(11);
        ui->btnOk->setVisible(true);
        break;
    case 11:
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\t Une fois de plus, vous tentez d’oublier la misère ambiante en vous alcoolisant. Vous perdez 10% de votre total de vie. ");
        setCompteur(13);
        ui->btnOk->setVisible(true);
        this->scrollMax();
        break;
    case 12:
        if (controle == 0)
        {
            ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\t « Trop cool ! Ça fait vraiment plaisir de croiser une personne aussi concernée ! Tu sais, dans une société ou tout l’monde est d’plus en plus égocentrique et individualiste, c’est difficile de trouver des gens qui s’intéressent à autre chose qu’eux même et… ». Vous décidez de ne plus l’écouter à partir de là mais en résumé, il vous fait tout un blabla de 25 minutes sur pourquoi son association est mieux que celle des autres, que dans d’autres associations, des gens hauts placés gagnent de l’argent sur le dos de ceux qui donnent. Bref il vous raconte toute sa vie et vous vous en foutez éperdument. Mais vous avez décidé d’être poli. Pas de chance, votre gentillesse ne vous aura rien apporté à part le fait de vous faire emmerder en pleine après-midi. ");
            setCompteur(13);
            query->insertSituation12(1);
            this->scrollMax();
            controle=1;
        }
        else
        {
            win = jeu->attaquePr();
            if (win==true)
            {
                ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tVous lui expliquez que vous ne vous intéressez pas du tout à ça, que vous ne valez pas mieux que n’importe qui d’autre et que même si vous aviez les moyens financiers de nourrir toute l’Afrique pendant 3 ans, vous ne le feriez pas parce que vous auriez trop la flemme de faire les démarches administratives. Vous finissez votre discours en allumant une cigarette. Vous lui crachez la fumer au visage de manière très peu respectueuse et partez comme un prince, fier de vous. Non, vous n’êtes pas quelqu’un de gentil… par contre, vous êtes sacrément badass !");
                ui->LChoose->setText("Vous gagnez le combat");
                query->insertRandom12(1);
                this->scrollMax();
            }
            else if (win == false)
            {
                ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tVous lui expliquez que vous n’avez pas vraiment les moyens de vous engager dans quelque chose de ce type. Il vous coupe la parole et vous dit que ce n’est pas grave, que ce qui compte c’est de donner, même si c’est une petite somme. « Et puis tu sais, au pire, si vraiment un mois ça va pas, tu peux toujours suspendre tes dons avec un simple coup d’fil ! ». Bref, votre tentative d’évitement est un échec critique et il vous tient la jambe pendant 15 minutes pour que finalement vous finissiez par lui dire « merci mais non merci ». Vous auriez peut-être dû rester chez vous, finalement. ");
                ui->LChoose->setText("Vous perdez le combat");
                query->insertRandom12(2);
                this->scrollMax();
            }
        }
        ui->btnOk->setVisible(true);
        break;
    case 14:
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tT’es sérieux ? Regarde-toi, à rien foutre de ta vie à part t’envoyer des shots dans la face du matin au soir. La seule et unique manière pour que ce soit pire, ce serait qu’tu t’fasses défoncer par un bus en sortant d’chez toi… Et pas sûr que ce serait un mal, en fait. »");
        setCompteur(15);
        ui->btnOk->setVisible(true);
        this->scrollMax();
        break;
    case 15:
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tL’homme se précipite soudainement sur vous et vous frappe de toutes ses forces avec le dictionnaire qui traînait sur votre table de chevet. Vous vous demandez ce que ce bouquin foutait à cet endroit, mais vous n’avez pas le temps de penser à une réponse que vous n’êtes déjà plus conscient.");
        ui->btnOk->setVisible(true);
        this->scrollMax();
        break;
    case 16:
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\t- Dommage, j’suis sûr que ça pourrait t’intéresser. »");
        ui->btnOk->setVisible(true);
        this->scrollMax();
        break;
    case 17:
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\t- Très bien. C’est maintenant qu’ça devient fun ! »");
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tIl vous attrape par le bras et se lance dans un monologue interminable");
        ui->btnOk->setVisible(true);
        this->scrollMax();
        this->end();
        this->scrollMax();
        break;
    case 18:
        ui->LChoose->setText("Cliquez sur Suite de l'histoire");
        ui->btnOk->setVisible(true);
        break;
    case 19:
        /*
        sik->pause();
        music = new QMediaPlayer();
        li = new QMediaPlaylist();

        li->addMedia(QUrl("Kid.mp3"));
        li->setPlaybackMode(QMediaPlaylist::Loop);

        music->setMedia(li);
        music->setVolume(10);

        music->play();
        */
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\t Cette chanson vous apaise. L’histoire d’un timbré qui a parfaitement conscience de l’être. « I found a monster in me when I lost my cool ». Oui, on peut dire ça comme ça. Vous vous dites que même sans perdre votre « cool », vous avez finalement réussi à trouver un monstre tout au fond de votre âme. Un type qui échoue dans un peu tout ce qu’il entreprend même s’il fait des efforts. « I love the dark, maybe we can make it darker, gimme a marker ». Le marqueur en question à un barillet de 6 balles et se trouve dans votre main droite. Vous vous dites que c’est un peu extrême comme marqueur, mais vous avez l’impression que cette ligne en particulier a été écrite pour ce moment précis. ");
        ui->btnOk->setVisible(true);
        this->scrollMax();
        break;
    case 22:
        ui->btnRandom->setText("gachette");
        ui->LChoose->setText("cliquez sur la gachette autant que vous voulez ! sinon cliquez sur l'autre réponse.");
        ui->btnRandom->setVisible(true);
        ui->btnOk->setVisible(false);
        this->scrollMax();
        break;

    }
}

void FenetreJeu::on_RbChoose2_clicked()
{
    switch (getCompteur()){
    case 2:
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tWow… Effectivement, le Rouge fait encore effet. » ");
        this->scrollMax();
        ui->btnOk->setVisible(true);
        break;
    case 3:
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\t« Puisque vous n’avez aucune intention de travailler, vous pouvez rentrer chez vous ! ». Ce n’est pas passé. Vous vous demandez depuis quand ce type à un semblant d’autorité et vous vous exécutez sans poser de question. Vous vous contentez d’un « bonne journée » avec un sourire (parce que vous avez été bien élevé) et sortez de la salle. ");
        this->scrollMax();
        ui->btnOk->setVisible(true);
        break;
    case 4:
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\t« Cool ! A tout d’suite, gros ! Tu gères ! La première tournée est pour moi haha ! ». Vous refermez votre téléphone et vous vous dites que cet appel était beaucoup trop long pour pas grand-chose… mais vous allez avoir une bière gratuite, c’est déjà ça. ");
        this->scrollMax();
        ui->btnOk->setVisible(true);
        break;
    case 5:
        query->insertSituation5(1);
        setCompteur(7);
        ui->btnOk->setVisible(true);
        break;
    case 7:
    {
        if (controle==0)
        {
            ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tVous lancez l'attaque pettage rupteur\n\tVous commencer a vous énerver, faite un lancé de dée pour vous en sortir");
            win=jeu->attaquePr();
            this->scrollMax();
            if (win==true)
            {
                ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tC’est fait, vous êtes arrivé jusqu’à elle, ne vous êtes pas étalé et n’avez pas vomi. Il ne vous reste plus qu’à trouver les bons mots !");
                this->scrollMax();
                ui->RbChoose1->setText("« Écoute, j’vais être direct, j’suis grave bourré et tu m’plais carrément.");
                ui->RbChoose2->setText(" « T’as d’beaux yeux, tu sais ? ». Vous êtes bourré… trop bourré, peut-être. ");
                ui->RbChoose3->setText(" « Euh… salut. Ça t’dirait qu’on aille fumer dehors ? On s’entend plus parler, ici.");
                controle=1;
            }
            else if (win == false)
            {
                ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tVotre concentration ne vous aura décidément pas aidé. Vous avez à cœur de lui citer un passage de « Roméo et Juliette » en anglais. Mais les seuls et uniques sons qui sortent de votre bouche sont parfaitement incompréhensibles pour le commun des mortels et ressemble approximativement à « qmsdkcjvnbsospanfkgùq ». Elle est foudroyée par la peur, vous dit « tu devrais boire un verre d’eau, ça t’ferait sûrement du bien » avec un sourire gêné et se dirige vers la sortie du bar. Vous vous dites que ça aurait pu être pire. Vous êtes clairement trop bourré, ça n’aurait pas pu être pire.");
                this->scrollMax();
            }
        }
        else if (controle ==1)
        {
            ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\t: - Ah ouais… on en est là. » vous dit-elle avec du dédain plein la voix avant d’aller vers la sortie du bar. ");
            this->scrollMax();
            query->insertSituation7(1);
        }
        ui->btnOk->setVisible(true);
        break;
    }
    case 8:
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\t« ce verre vous fait du bien. Beaucoup de bien. Vous gagnez 15% de vie. ");
        jeu->situation8_2();
        ui->PbPv->setValue(jeu->gamer->getPv());
        this->scrollMax();
        ui->btnOk->setVisible(true);
        break;
    case 10:
        ui->btnOk->setVisible(true);
        ui->LChoose->setText("Cliquez sur poursuivre l'histoire");
        break;
    case 11:
        setCompteur(14);
        ui->btnOk->setVisible(true);
        break;
    case 12:
        ui->LChoose->setText("COMBAT");
        if (controle == 0)
        {
            ui->RbChoose1->setText("Attaque : CreveCoeur");
            ui->RbChoose2->setText("Attaque : pettage rupteur");
            ui->RbChoose3->setText("Attaque : fist");
            controle=1;
        }
        else
        {
            win = jeu->attaquePr();
            if (win==true)
            {
                ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tVous lui expliquez que vous ne vous intéressez pas du tout à ça, que vous ne valez pas mieux que n’importe qui d’autre et que même si vous aviez les moyens financiers de nourrir toute l’Afrique pendant 3 ans, vous ne le feriez pas parce que vous auriez trop la flemme de faire les démarches administratives. Vous finissez votre discours en allumant une cigarette. Vous lui crachez la fumer au visage de manière très peu respectueuse et partez comme un prince, fier de vous. Non, vous n’êtes pas quelqu’un de gentil… par contre, vous êtes sacrément badass !");
                ui->LChoose->setText("Vous gagnez le combat");
                query->insertRandom12(1);
                this->scrollMax();
            }
            else if (win == false)
            {
                ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tVous lui expliquez que vous n’avez pas vraiment les moyens de vous engager dans quelque chose de ce type. Il vous coupe la parole et vous dit que ce n’est pas grave, que ce qui compte c’est de donner, même si c’est une petite somme. « Et puis tu sais, au pire, si vraiment un mois ça va pas, tu peux toujours suspendre tes dons avec un simple coup d’fil ! ». Bref, votre tentative d’évitement est un échec critique et il vous tient la jambe pendant 15 minutes pour que finalement vous finissiez par lui dire « merci mais non merci ». Vous auriez peut-être dû rester chez vous, finalement. ");
                ui->LChoose->setText("Vous perdez le combat");
                query->insertRandom12(2);
                this->scrollMax();
            }
        }
        ui->btnOk->setVisible(true);
        break;
    case 14:
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tAu moins t’es lucide, c’est déjà ça. Ça règle pas les problèmes mais s’en rendre compte, c’est déjà quelque chose. »");
        setCompteur(15);
        this->scrollMax();
        ui->btnOk->setVisible(true);
        break;
    case 15:
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tL’homme se précipite soudainement sur vous et vous frappe de toutes ses forces avec le dictionnaire qui traînait sur votre table de chevet. Vous vous demandez ce que ce bouquin foutait à cet endroit, mais vous n’avez pas le temps de penser à une réponse que vous n’êtes déjà plus conscient.");
        ui->btnOk->setVisible(true);
        this->scrollMax();
        break;
    case 16:
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\t- T’es un marrant toi. On t’a jamais appris à pas répondre de la merde aux gens qu’tu connaissais pas ? »");
        ui->btnOk->setVisible(true);
        this->scrollMax();
        break;
    case 17:
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\t- Disons que c’que j’vais t’dire risque de pas t’faire plaisir. Mais à part ça aucune ‘embrouille’ à proprement parlé. »");
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tIl vous attrape par le bras et se lance dans un monologue interminable");
        ui->btnOk->setVisible(true);
        this->scrollMax();
        this->end();
        this->scrollMax();
        break;
    case 18:
        setCompteur(22);
        ui->btnOk->setVisible(true);
        ui->LChoose->setText("Cliquez sur Suite de l'histoire");
        break;
    case 19:
        /*
        sik->pause();
        music = new QMediaPlayer();
        li = new QMediaPlaylist();

        li->addMedia(QUrl("orange.mp3"));
        li->setPlaybackMode(QMediaPlaylist::Loop);

        music->setMedia(li);
        music->setVolume(10);

        music->play();*/
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\t Vous avez choisi une chanson presque totalement instrumentale. Les rares paroles que l’on entend sont des samples utilisés dans des films en noir et blanc. Tout un symbole. Un univers cinématographique pour un homme qui aura vécu la majorité de sa vie à la regarder défiler devant ses propres yeux. Si vous n’aviez pas une arme létale dans l’autre main, la dimension métaphorique de la chose vous rendrait presque tout chose… Le titre de la chanson et son univers sombre résume à eux seuls votre vie. Si on ajoute le fait que la chanson s’arrête d’un coup sec, on pourrait presque faire une analyse de professeur de français insupportable en mettant en parallèle le final de cette chanson et le dernier acte de votre vie. Mais laissons les professeurs où ils sont… à la machine à café. ");
        ui->btnOk->setVisible(true);
        this->scrollMax();
        break;
    case 22:
        ui->textEdit->setText(ui->textEdit->toPlainText() +"\n"+"\n"+ query->selectEnd2());
        ui->LChoose->setText("Cliquez sur suite de l'histoire");
        ui->btnOk->setVisible(true);
        this->scrollMax();
        break;
    }

}

void FenetreJeu::on_RbChoose3_clicked()
{
    switch (getCompteur()){
    case 2:
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tBon… ben c’est pas grave ». Oui, votre camarade est ce qu’on pourrait communément appeler « une victime ». ");
        this->scrollMax();
        ui->btnOk->setVisible(true);
        break;
    case 3:
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\t « C’est ça, faites comme si vous n’aviez rien entendu ! ». Ça aurait pu être pire.  ");
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tVous passez une journée en enfer. Vous avez chaud, froid, êtes fatigué, en avez marre de tout (surtout des cours) et de tout le monde. Vous ne pensez qu’à une chose : rentrer chez vous.  ");
        this->scrollMax();
        ui->btnOk->setVisible(true);
        break;
    case 4:
        query->insertSituation4(1);
        setCompteur(9);
        ui->btnOk->setVisible(true);
        break;
    case 7:
        if (controle==0)
        {
            ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tVous lancez l'attaque Fist");
            this->scrollMax();
            win=jeu->attaqueF();
            if (win==true)
            {
                ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tC’est fait, vous êtes arrivé jusqu’à elle, ne vous êtes pas étalé et n’avez pas vomi. Il ne vous reste plus qu’à trouver les bons mots !");
                this->scrollMax();
                ui->RbChoose1->setText("« Écoute, j’vais être direct, j’suis grave bourré et tu m’plais carrément.");
                ui->RbChoose2->setText(" « T’as d’beaux yeux, tu sais ? ». Vous êtes bourré… trop bourré, peut-être. ");
                ui->RbChoose3->setText(" « Euh… salut. Ça t’dirait qu’on aille fumer dehors ? On s’entend plus parler, ici.");
                controle=1;
            }
            else if (win == false)
            {
                ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tVotre concentration ne vous aura décidément pas aidé. Vous avez à cœur de lui citer un passage de « Roméo et Juliette » en anglais. Mais les seuls et uniques sons qui sortent de votre bouche sont parfaitement incompréhensibles pour le commun des mortels et ressemble approximativement à « qmsdkcjvnbsospanfkgùq ». Elle est foudroyée par la peur, vous dit « tu devrais boire un verre d’eau, ça t’ferait sûrement du bien » avec un sourire gêné et se dirige vers la sortie du bar. Vous vous dites que ça aurait pu être pire. Vous êtes clairement trop bourré, ça n’aurait pas pu être pire.");
                this->scrollMax();
            }
        }
        else if (controle==1)
        {
            ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\t - Bien tenter, l’approche, mais j’fume pas, en fait », vous dit-elle avec un sourire avant de s’éloigner. Tout ça pour ça. C’est ce qu’on appelle la lose. ");
            this->scrollMax();
            query->insertSituation7(1);
        }
        ui->btnOk->setVisible(true);
        break;
    case 12:
        if (controle == 0)
        {
            ui->LChoose->setText("Cliquez sur poursuivre l'histoire");
            ui->btnOk->setVisible(true);
            query->insertSituation12(1);
            controle=1;
        }
        else
        {
            win = jeu->attaquePr();
            if (win==true)
            {
                ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tVous lui expliquez que vous ne vous intéressez pas du tout à ça, que vous ne valez pas mieux que n’importe qui d’autre et que même si vous aviez les moyens financiers de nourrir toute l’Afrique pendant 3 ans, vous ne le feriez pas parce que vous auriez trop la flemme de faire les démarches administratives. Vous finissez votre discours en allumant une cigarette. Vous lui crachez la fumer au visage de manière très peu respectueuse et partez comme un prince, fier de vous. Non, vous n’êtes pas quelqu’un de gentil… par contre, vous êtes sacrément badass !");
                ui->LChoose->setText("Vous gagnez le combat");
                query->insertRandom12(1);
                this->scrollMax();
            }
            else if (win == false)
            {
                ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tVous lui expliquez que vous n’avez pas vraiment les moyens de vous engager dans quelque chose de ce type. Il vous coupe la parole et vous dit que ce n’est pas grave, que ce qui compte c’est de donner, même si c’est une petite somme. « Et puis tu sais, au pire, si vraiment un mois ça va pas, tu peux toujours suspendre tes dons avec un simple coup d’fil ! ». Bref, votre tentative d’évitement est un échec critique et il vous tient la jambe pendant 15 minutes pour que finalement vous finissiez par lui dire « merci mais non merci ». Vous auriez peut-être dû rester chez vous, finalement. ");
                ui->LChoose->setText("Vous perdez le combat");
                query->insertRandom12(2);
                this->scrollMax();
            }
        }
        ui->btnOk->setVisible(true);
        break;
    case 14:
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tPlus facile de rien faire que d’affronter la vie, hein ? »");
        setCompteur(15);
        ui->btnOk->setVisible(true);
        this->scrollMax();
        break;
    case 15:
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tL’homme se précipite soudainement sur vous et vous frappe de toutes ses forces avec le dictionnaire qui traînait sur votre table de chevet. Vous vous demandez ce que ce bouquin foutait à cet endroit, mais vous n’avez pas le temps de penser à une réponse que vous n’êtes déjà plus conscient.");
        ui->btnOk->setVisible(true);
        this->scrollMax();
        break;
    case 16:
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\t- T’en as pas marre de rien faire, rien dire ? »");
        ui->btnOk->setVisible(true);
        this->scrollMax();
        break;
    case 17:
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\t- Ok, une fois d’plus tu choisis d’être lâche. Certes. »");
        query->insertSituation17(3);
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tIl vous attrape par le bras et se lance dans un monologue interminable");
        ui->btnOk->setVisible(true);
        query->insertSituation17(1);
        this->scrollMax();
        this->end();
        this->scrollMax();
        break;
    case 19:
        /*
        sik->pause();
        music = new QMediaPlayer();
        li = new QMediaPlaylist();

        li->addMedia(QUrl("soad.mp3"));
        li->setPlaybackMode(QMediaPlaylist::Loop);

        music->setMedia(li);
        music->setVolume(10);

        music->play();*/
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\t Après les premières notes de guitare, vous vous dites que cette chanson devrait être l’hymne de la vie. Le début en crescendo sans aucune percussion, le moment ou Serj Tankian se met à hurler pour finir sur l’un des plus beaux moments que la musique ait connue. Une sorte d’anarchie parfaitement harmonique, la violence contre balancée à la perfection avec d’autres passages de la chanson, bref, la parfaite représentation de ce qu’est votre vie : des moments très durs qui sont adoucis par différents palliatifs tels que l’alcool ou la drogue. « Why have you forsaken me ? ». Cette phrase tellement lourde de sens que vous poseriez à Dieu si vous étiez croyant, cette phrase que vous vous posez à chaque que vous croisez votre propre regard dans une glace, cette phrase que vous vous posez car vous n’avez pas le courage d’admettre que si votre vie est tellement inintéressante, c’est entièrement votre faute.");
        ui->btnOk->setVisible(true);
        this->scrollMax();
        break;
    }

}

void FenetreJeu::checked()
{
    ui->RbChoose1->setAutoExclusive(false);
    ui->RbChoose1->setChecked(false);
    ui->RbChoose1->setAutoExclusive(true);
    ui->RbChoose2->setAutoExclusive(false);
    ui->RbChoose2->setChecked(false);
    ui->RbChoose2->setAutoExclusive(true);
    ui->RbChoose3->setAutoExclusive(false);
    ui->RbChoose3->setChecked(false);
    ui->RbChoose3->setAutoExclusive(true);
}

void FenetreJeu::end()
{
    int option4 = query->selectOption4();
    int option5 = query->selectOption5();
    int option7 = query->selectOption7();
    int option12 = query->selectOption12();
    int option17 = query->selectOption17();
    int random12 = query->selectRandom12();

    if (option4 == 1 )
    {
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tQu’est ce que t’avais à perdre à aller à cette soirée à la con avec Lucas ? De l’argent, et après ? Tu comptes te faire enterrer dans un cercueil de diamants ? Franchement, c’est quoi ton problème ? Même quand tes ‘potes’ viennent te chercher pour te faire sortir de ton trou, t’es pas foutu d’te bouger l’cul. Tu mérites même pas ces gens-là. La seule chose que tu mérites, c’est c’qui t’arrive dans la face au jour le jour. Rien. Ta solitude et ton portable plongé dans le coma depuis des mois. Voilà c’que tu mérites. ");
    }

    if (option5 == 1)
    {
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\t Tu m’expliques ce que c’est ton excuse pour ne pas être allé voir la fille d’tes rêves pendant cette fameuse soirée ? Cherche pas trop longtemps non plus, la seule réponse acceptable dans cette situation est : ‘je n’suis rien d’plus qu’un gros lâche qui préfère bader sur ma vie avec 2 grammes plutôt que d’me bouger l’cul’. Cette nana, t’en rêves la nuit, tu t’imagines déjà entrain d’te balader sur les quais, main dans la main avec elle, comment tu lui passerais la main dans les cheveux avec un sourire et une p’tite brise vous ébouriffant la crinière à tous les deux, comment tu la demanderais en mariage, et tout un tas d’autres conneries dans ce style qui arriveront jamais parce que… parce que t’es toi, tout simplement. Non pas parce que t’es un mauvais bougre, mais juste parce que justement, t’as décidé d’jamais rien faire.");
    }

    if (option7 == 1)
    {
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\t Et quand tu fais des efforts pour te bouger l’train, sortir et aller voir des gens, c’est quand même à moitié un échec. T’as lu « The Great Gatsby » ? Bien sûr que non, tu l’as pas lu. T’as regardé la version avec DiCaprio parce que t’es un branleur. Mais en gros, ton histoire avec Micheline est très Gatsby dans l’âme. Un mec à la recherche perpétuelle d’un truc qui n’est plus là depuis bien longtemps… sauf que lui, il est beau, riche et qu’à un moment, il arrive à s’mettre avec la fille, même s’il la perd à la fin. Toi, t’as même pas eu ces quelques moments d’bonheur avec ta donzelle. Ouais mon pote, ‘Life is a bitch’. Elle l’a toujours été et le sera toujours. ‘Deal with it’. ");
    }

    if (option12 == 1)
    {
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\t Même face à un sous être genre le crasseux qui t’as accosté sur la place, t’es pas capable de réagir et d’dire c’que tu penses. C’est pas triste ça, sincèrement ? Tu préfères te laisser emmerder plutôt que d’te faire entendre. ");
    }

    if (random12 == 1)
    {
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\t Au moins t’auras réussi à t’débarasser d’l’autre blaireau qui est venu t’emmerder sur la place, c’est déjà ça ! En même temps, vu l’état du débris, on peut pas dire que c’était un tour de force phénoménal non plus, hein, donc évite d’être trop fier de toi.");
    }
    else if (random12 == 2)
    {
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\t La seule fois que t’aurais pu être un tant soit peu un bonhomme ça s’est conclu par un échec critique et t’as fini par te faire emmerder pendant 3 plombes par un clampin qui voulait sauver l’monde. Si c’est pas la lose. Certains diraient qu’au moins t’as essayé d’te défendre. Moi j’me contenterais d’te dire que tu t’es fait victimiser par un hippie bien sale avec dreadlocks… et c’est plutôt humiliant, comme situation");
    }

    if (option17 == 1)
    {
        ui->textEdit->setText(ui->textEdit->toPlainText()+"\n\tEt même quand un mec débarque pour te « sortir » de ta merde t’es capable de n’pas lui parler et d’continuer ta p’tite vie misérable sans rien lui demander. J’l’ai déjà dit mais… c’est pas épuisant d’être à c’point spectateur de sa propre vie… ? T’as pas l’impression d’rater quelque chose ? L’impression de servir à rien ? Ça t’plait d’être comme ça ? Inerte ? Tu t’rends compte qu’on a déjà vu des palourdes plus épanouies qu’toi ? ");
    }
}

void FenetreJeu::on_pushButton_clicked()
{
    if (sik->volume()==0)
    {
        sik->setVolume(10);
    }
    else
    {
        sik->setVolume(0);
    }
}
